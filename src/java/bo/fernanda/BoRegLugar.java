
package bo.fernanda;

import dao.fernanda.DAORegLugar;
import java.io.PrintWriter;
import modelo.fernanda.ModeloLugar;

/**
 *
 * @author Abraham
 */
public class BoRegLugar {
    //metodo reglas para validar las reglas de negocio
    public void reglas(ModeloLugar modelo,PrintWriter out){
    //crear objeto de DAORegLugar y mandar parametros
        DAORegLugar dao= new DAORegLugar();
        dao.registrar(modelo, out);
                
}
    
}
