
package bo.fernanda;

import dao.fernanda.DAOEditarDatos;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletRequest;
import modelo.fernanda.ModeloUsuario;

/**
 *
 * @author Abraham
 */
public class BoEditarDatos {
    
    //metodo para validar las reglas de negocio
    public void reglas(ModeloUsuario model,PrintWriter out,HttpServletRequest request){
        //creamos un objeto de la clase DAOEditarDatos
        DAOEditarDatos dao= new DAOEditarDatos();
        //mandamos a dao el modelo,el out y request
        dao.actualizar(model, out, request);
    }
}
