
package bo.fernanda;

import dao.fernanda.DAOLogeo;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletRequest;
import modelo.fernanda.ModeloLogeo;

/**
 *
 * @author Abraham
 */
public class BoLogeo {
    
    //aqui se validaran las reglas de negocio
    public void reglas(ModeloLogeo modelo,PrintWriter out,HttpServletRequest request){
        //creacion de objeto DaoLogeo
        DAOLogeo dao= new DAOLogeo();
        //se manda al metodo inicioSesion el LOGEOmodelo
        dao.inicioSesion(modelo,out,request);
    }
    
}
