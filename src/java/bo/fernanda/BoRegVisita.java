package bo.fernanda;
import conexion.Conexion;
import controlador.fernanda.Modulo1;
import dao.fernanda.DAORegVisita;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.http.HttpServletRequest;
import modelo.fernanda.ModeloVisita;

/**
 *
 * @author Abraham
 */
public class BoRegVisita {
      //metodo para validar las reglas de negocio
      public void reglas(ModeloVisita modelo,PrintWriter out,HttpServletRequest request){
        //despues buscamos al id con el nombre del lugar , y lo mandamos al SETTER ID_LUGAR
        String id=consultaId(modelo.getNombre_lugar());
        modelo.setId_lugar(id);
        modelo.setFecha(fecha(modelo));
        // se crea un objeto de DAO PARA REGISTRAR FINALMENTE LA VISITA
        DAORegVisita dao= new DAORegVisita();
        //mandamos modelo y out a la dao
        dao.registrar(modelo, out,request);
        
    }
    
      public String consultaId(String nombre){
           String id="";
        try {
//se definen los stament para realizar la consulta a la base de datos
   ResultSet cdr = null;
Statement sentenciaSQL = null;
Conexion conecta = new Conexion();
conecta.Conectar();
sentenciaSQL = conecta.getSentenciaSQL();
final String sqlId="SELECT IDLUGAR FROM LUGAR WHERE NOMBRE = '"+nombre+"' ";
cdr=sentenciaSQL.executeQuery(sqlId);
while (cdr.next()){
    id=cdr.getString("IDLUGAR");
}
conecta.cerrar();
           return id;
        } catch (Exception ex) {
            System.out.println("error de BORegVsiitia"+ex);
        }
        return id;
    }
      
      public String fecha(ModeloVisita  modelo){
          String fechaOriginal=modelo.getFecha();
          String ano=fechaOriginal.substring(0,4);
          String mes=fechaOriginal.substring(5,7);
          String dia=fechaOriginal.substring(8,10);
          String dia1="";
          if (dia.length()==1){
              dia1="0"+dia;
          }else{
              dia1=dia;
          }
          String fechaReves=dia1+"/"+mes+"/"+ano;
          return fechaReves;
      }
}
