
package bo.fernanda;

import dao.fernanda.DAOCambiarFoto;
import modelo.fernanda.ModeloUsuario;

/**
 *
 * @author abraham
 */
public class BoCambiarFoto {
    public void reglas(ModeloUsuario modelo){
        //metodo para validar reglas de negocio
        //Creamos un objeto de dao y le mandamos el modelo
        DAOCambiarFoto dao= new DAOCambiarFoto();
        dao.guardarFoto(modelo);
    }
}
