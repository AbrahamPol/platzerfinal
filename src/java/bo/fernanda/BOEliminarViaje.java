
package bo.fernanda;

import dao.fernanda.DAOEliminarViaje;
import modelo.fernanda.ModeloViaje;

/**
 *
 * @author Abraham
 */
public class BOEliminarViaje {

    public void reglas(ModeloViaje modelo) {
        //mandar  a dao el modelo para eliminar viaje
        DAOEliminarViaje dao= new DAOEliminarViaje();
        dao.eliminar(modelo);
    }
    
}
