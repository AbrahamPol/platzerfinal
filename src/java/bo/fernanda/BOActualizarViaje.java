
package bo.fernanda;

import dao.fernanda.DAOActivarUsuario;
import dao.fernanda.DAOActualizarViaje;
import dao.fernanda.DAOEliminarViaje;
import modelo.fernanda.ModeloViaje;

/**
 *
 * @author Abraham
 */
public class BOActualizarViaje {

    public void reglas(ModeloViaje modelo) {
      //mandar el modelo a la dao para actualizar el viaje
        DAOActualizarViaje dao = new DAOActualizarViaje();
        dao.actualizar(modelo);
    }
    
}
