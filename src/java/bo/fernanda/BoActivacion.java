
package bo.fernanda;

import dao.fernanda.DAOActivarUsuario;
import modelo.fernanda.ModeloActivacion;

/**
 *
 * @author Abraham
 */
public class BoActivacion {
    //aqui no validamos nada , es por eso que se manda limpio el objeto de activacion
    public void reglas(ModeloActivacion activacion){
        DAOActivarUsuario dao= new DAOActivarUsuario();
        dao.estadoActivo(activacion);
    }
}
