
package bo.fernanda;

import conexion.Conexion;
import dao.fernanda.DAORegistroUsuario;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.Statement;

import modelo.fernanda.ModeloUsuario;
import java.util.Properties;
import java.util.Random;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.swing.JOptionPane;
/**
 *
 * @author Abraham
 */
public class BoUsuario {
   
    //aqui pondremos los datos en mayusculas, etc, para pasarlos limpios a la dao
    // e insertar en la base de datos
    public  void reglas(ModeloUsuario usuario,PrintWriter out){
        if (noRepetido(usuario)){
       String nombreMayusculas =usuario.getNombre();
       nombreMayusculas.toUpperCase();
       usuario.setNombre(nombreMayusculas);
       String paternoMayusculas =usuario.getPaterno();
       paternoMayusculas.toUpperCase();
       usuario.setPaterno(paternoMayusculas);
       //Aqui Validamos que la contraseña tenga al menos 6 digitos y maximo 14
       int cantidad =usuario.getContrasena().length();  
       if (cantidad>=6 && cantidad <=14){
         //ya validado mandamos a la DAO el objeto para insertar en la base de datos
           DAORegistroUsuario ru= new DAORegistroUsuario();
           ru.RegistroUsuario(usuario,out);
           //aqui mandamos el correo de confirmacion al usuario
           mandarCorreo(usuario,out);
  
       }else{
           out.print("Tu contraseña debe de contener de 6 a 12 caracteres");
       }
        }else{
             out.print("Ya existe una cuenta con ese correo");
        }
    
 
    }
    
    //mismo metodo para no repetir al usuario
    public boolean noRepetido(ModeloUsuario usuario){
                try {
    ResultSet cdr = null;
    Statement sentenciaSQL = null;
    Conexion conecta = new Conexion();
    conecta.Conectar();
    sentenciaSQL = conecta.getSentenciaSQL();
    String sql2="select * from usuario where email='"+usuario.getEmail()+"'";
    cdr=sentenciaSQL.executeQuery(sql2);
    int cont=0;
    while (cdr.next()){
        cont++;
    }
    conecta.cerrar();
    if (cont==0){
       return true;
    }else {
        return false;
    }
    

        } catch (Exception e) {
            System.out.println("e"+e);
        }
       return false;
    }
    
    //Se hace la configuracion para enviar correo
    public static void mandarCorreo(ModeloUsuario usuario,PrintWriter out){
    
      // El correo gmail de envío(PLATZER)
      String correoEnvia = "platzer.social@gmail.com";
      String claveCorreo = "platzer3232";
      // La configuración para enviar correo
      Properties properties = new Properties();
      properties.put("mail.smtp.host", "smtp.gmail.com");
      properties.put("mail.smtp.port", "587");
      properties.put("mail.smtp.starttls.enable", "true");
      properties.put("mail.smtp.auth", "true");
      properties.put("mail.user", correoEnvia);
      properties.put("mail.password", claveCorreo);
      // Obtener la sesion
      Session session = Session.getInstance(properties, null);
      int aviso = 0;
      try {
       // Crear el cuerpo del mensaje
        MimeMessage mimeMessage = new MimeMessage(session);
       // Agregar quien envía el correo
        mimeMessage.setFrom(new InternetAddress(correoEnvia, "PLATZER"));
       // Los destinatarios (El usuario)
        InternetAddress[] internetAddresses = {new InternetAddress(usuario.getEmail())};
       // Agregar los destinatarios al mensaje
        mimeMessage.setRecipients(Message.RecipientType.TO,internetAddresses);
       // Agregar el asunto al correo
        mimeMessage.setSubject("Codigo de activacion");
       // Creo la parte del mensaje
        MimeBodyPart mimeBodyPart = new MimeBodyPart();
      mimeBodyPart.setText("Copia el siguiente link para activar tu cuenta:   "+hacerCodigo(usuario));
//        MimeBodyPart mimeBodyPartAdjunto = new MimeBodyPart();
//        mimeBodyPartAdjunto.attachFile("img/login.png");
       //Crear el multipart para agregar la parte del mensaje anterior
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(mimeBodyPart);
//        multipart.addBodyPart(mimeBodyPartAdjunto);
       // Agregar el multipart al cuerpo del mensaje
        mimeMessage.setContent(multipart);
       // Enviar el mensaje
        Transport transport = session.getTransport("smtp");
        transport.connect(correoEnvia, claveCorreo);
        transport.sendMessage(mimeMessage, mimeMessage.getAllRecipients());
        transport.close();
      } catch (Exception ex) {
       ex.printStackTrace();
       JOptionPane.showMessageDialog(null, "Error: "+ex.getMessage());
       aviso = 1;
      }
      if (aviso==0) {
//              JOptionPane.showMessageDialog(null, "¡Correo electrónico enviado exitosamente!");
       out.print("Tu registro a sido exitoso, para activar tu cuenta ingresa a tu correo y haz click en"
             + "la liga de confirmacion"); 
      }
     }

//este metodo nos genera la url de confirmacion para activar la cuenta del usuario
    public static String hacerCodigo(ModeloUsuario usuario){
        
            Random rnd=new Random();
            String abc="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            String cadena="";
            int m=0,pos=0,num;
            while (m<1) 
            {                
                pos=(int)(rnd.nextDouble()* abc.length() -1 + 0);
                num=(int) (rnd.nextDouble()* 9999 + 1000);
                cadena = cadena+abc.charAt(pos)+num;
                pos=(int)(rnd.nextDouble()* abc.length() -1 + 0);
                cadena = cadena+abc.charAt(pos);
                m++;
               
            }
             String liga="http://localhost:8080/ServActivarCuenta?codigo="+cadena+"&cl="+usuario.getId();
            return liga;
        }


}
