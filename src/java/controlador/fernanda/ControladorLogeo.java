
package controlador.fernanda;

import bo.fernanda.BoLogeo;
import conexion.Conexion;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.fernanda.ModeloLogeo;

/**
 *
 * @author Abraham
 */
@WebServlet(name = "Logeo", urlPatterns = {"/Logeo"})
public class ControladorLogeo extends HttpServlet {

    ResultSet cdr = null;
    Statement sentenciaSQL = null;
    Conexion conecta = new Conexion();
    
    
        public void init(ServletConfig config) throws ServletException{
       super.init(config);
       conecta.Conectar();
       sentenciaSQL = conecta.getSentenciaSQL();
    }
    
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

  
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
       //Recibo de datos 
         String email = request.getParameter("email");
        String password = request.getParameter("password");  //Mandar datos al modelo de Logeo
        ModeloLogeo modelo= new ModeloLogeo(email,password);
        //Crear objeto de BOLogeo
        BoLogeo bo= new BoLogeo();
        //mandar al metodo reglas, el objeto de modeloLogeo
        bo.reglas(modelo,out,request);
        out.close();
       }

 
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
