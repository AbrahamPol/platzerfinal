
package controlador.fernanda;

import conexion.Conexion;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Abraham
 */
@WebServlet(name = "ControladorVisitaFavorita", urlPatterns = {"/ControladorVisitaFavorita"})
public class ControladorVisitaFavorita extends HttpServlet {

        ResultSet cdr = null;
    Statement sentenciaSQL = null;
    Conexion conecta = new Conexion();
    
        public void init(ServletConfig config) throws ServletException{
       super.init(config);
       conecta.Conectar();
       sentenciaSQL = conecta.getSentenciaSQL();
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ControladorVisitaFavorita</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ControladorVisitaFavorita at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

  
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    
        try {
                int id_visita = Integer.parseInt(request.getParameter("id_visitaFer"));
        final String sql="insert into visitafavorita values(null,"+id_visita+")";
        sentenciaSQL.executeUpdate(sql);
        } catch (Exception e) {
            System.out.println(""+e);
        }
    }

   
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
