
package controlador.fernanda;

import conexion.Conexion;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name = "ServActUsu", urlPatterns = {"/ServActUsu"})
public class ServActUsu extends HttpServlet {
      ResultSet cdr = null;
    Statement sentenciaSQL = null;
    Conexion conecta = new Conexion();
    
    public void init(ServletConfig config) throws ServletException{
       super.init(config);
       conecta.Conectar();
       sentenciaSQL = conecta.getSentenciaSQL();
    }

   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServActUsu</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ServActUsu at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
              response.setContentType("text/html:charset=UTF-8");
        PrintWriter out = response.getWriter();
        String nombre = request.getParameter("nombre");
        System.out.println(nombre);
        try {
            cdr = sentenciaSQL.executeQuery("SELECT * FROM USUARIO WHERE NOMBRE = '"+nombre+"' ");
            while(cdr.next()){
                out.println("<center>"
                + "<div class=login-wrap id=regform>"
                + "<input type=text value='"+cdr.getString(2)+"' class=form-control id=nombreAT placeholder=Nombre* readonly=readonly pattern=[^0-9] title=\"Sin números perro\"><br/><br/>"
                + "<input type=text value="+cdr.getDouble(3)+" class=form-control id=costoAT placeholder=Costo*><br/><br/>"
                + "<input type=text value='"+cdr.getString(4)+"' class=form-control id=descripcionAT placeholder=Descripción*><br/><br/>"
                + "<select id=estTour>"
                + "<option value="+cdr.getString(5)+">"+cdr.getString(5)+"</option>"
                + "<option value=ALTA>ALTA</option>"
                + "<option value=BAJA>BAJA</option>"
                + "</select><br/><br/>"
                + "<input type=button  class=\"btn btn-theme btn-block\" value=\"Editar\" onclick=actualizarTour();><br/><br/>"                                                        
                + "</div>"
                + "<div id=actTour></div>"                
                + "</center>");
            }
        } catch (SQLException ex) {
            ex.getMessage();
            ex.printStackTrace();
        }
    }

 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

   
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
