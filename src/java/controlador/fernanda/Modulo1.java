/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.fernanda;

import conexion.Conexion;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fernanda
 */
public class Modulo1 {
    ResultSet cdr = null;
    Statement sentenciaSQL = null;
    Conexion conecta = new Conexion();
    private int id;
    
    public void consultaId(String nombre){
        try {
            conecta.Conectar();
            sentenciaSQL = conecta.getSentenciaSQL();
            cdr = sentenciaSQL.executeQuery("SELECT IDLUGAR FROM LUGAR WHERE NOMBRE = '"+nombre+"' ");
            while(cdr.next()){
                id = cdr.getInt(1);
            }
        } catch (SQLException ex) {
            ex.getMessage();
            ex.printStackTrace();
        }
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }
    
    
}
