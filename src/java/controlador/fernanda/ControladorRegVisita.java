
package controlador.fernanda;

import bo.fernanda.BoRegLugar;
import bo.fernanda.BoRegVisita;
import conexion.Conexion;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import modelo.fernanda.ModeloLugar;
import modelo.fernanda.ModeloVisita;

/**
 *
 * @author cuetl
 */
@WebServlet(name = "ServRegVisita", urlPatterns = {"/ServRegVisita"})
@MultipartConfig
public class ControladorRegVisita extends HttpServlet {
    
    ResultSet cdr = null;
    Statement sentenciaSQL = null;
    Conexion conecta = new Conexion();
    
    public void init(ServletConfig config) throws ServletException{
       super.init(config);
       conecta.Conectar();
       sentenciaSQL = conecta.getSentenciaSQL();
    }

     private final static Logger LOGGER =  Logger.getLogger(ControladorRegVisita.class.getCanonicalName());
    public ControladorRegVisita(){
        super();
    }

  
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServRegVisita</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ServRegVisita at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    PrintWriter out = response.getWriter();
        try {
            //en el get no se implementa mvc4 por que solo se requiera de una consulta, y no se
            //reciben parametros
            final String sql="SELECT * FROM LUGAR ";
            cdr = sentenciaSQL.executeQuery(sql);
            int cont=0;
            while (cdr.next()){
                cont++;   
            }
            cdr.close();
            if (cont==0){
                out.print("1");
            }else{
         out.println("<center>"
        + "<h3>Registrar Visita</h3><br/>"
        + "<div class=login-wrap id=regform>"
        + "<select id=nomlug onChange=mostrarLug();>"
        + "<option>Selecciona el Nombre</option>");
 final String sql2="SELECT NOMBRE FROM LUGAR ";
 cdr = sentenciaSQL.executeQuery(sql2); 
                while (cdr.next()){
out.println("<option value='"+cdr.getString(1)+"'>"+cdr.getString(1)+"</option>");                
                }//cierre de while
                out.println("</select><br/><br/>"
                        + "</div>");
                out.println("</center>");
            }
            
out.close();
        } catch (SQLException ex) {
            ex.getMessage();
            ex.printStackTrace();
        }

    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         String nombre="",fecha="",descripcion="";
         Float costo = null;
         int calificacion=0,estrellas=0;
         PrintWriter out5 = response.getWriter(); 
         String im="",im2="";
       for (Part part : request.getParts()) {//for para recorrer el tamaño del archivo
         try{
               
// IMPORTANTE DECLARAR ESTAS CONSTANTES
//EL STRING PATH DEBE IR CON LA RUTA DE TU PC
//request.getPart(aqui va el nombre que le envia ajax en el post ID)
//final String path ="C:/Users/Abraham/Documents/ABRAHAM/Integradora fer/Platzer/web/imgFer/";
//aqui cambia tu ruta y comenta la linea anterior
final String path = "C:/Users/Abraham/Documents/ABRAHAM/integradora final/platzer/web/imgFer/";

//C:\Users\Abraham\Documents\ABRAHAM\integradora final\platzer\web\imgFer
final Part filePart = request.getPart("imagenVisitaFer");
final String fileName = getFileName(filePart);
//OutPutStream y Stream son necesarias para hacer el proceso de datos
OutputStream out = null;
InputStream filecontent = null;
//PrintWrite para sobreescribir el path del archivo
final PrintWriter writer = response.getWriter();

im=request.getParameter("imagenVisitaFer");//se recibe el nombre del archivo del formulario


///////////////////////////////CODIGO PARA GUARDAR LA IMAGEN///////////////////////////////////////////////7
 try {
out = new FileOutputStream(new File(path + File.separator
+ fileName));
filecontent = filePart.getInputStream();
int read = 0;
final byte[] bytes = new byte[1024];
while ((read = filecontent.read(bytes)) != -1) {
out.write(bytes, 0, read);
}
im2="../imgFer/"+fileName;
    } catch (FileNotFoundException fne) {}


   ///////////////////////////////////////////////////////////////////7
   
        response.setContentType("text/html:charset=UTF-8");
        //recibo de parametros
        nombre = request.getParameter("nombrev").toUpperCase();
        fecha = request.getParameter("fechalugar");
         costo = Float.parseFloat(request.getParameter("costolugar"));
         descripcion = request.getParameter("descripcionlugar");
//         calificacion = Integer.parseInt(request.getParameter("calificacionlugar"));
        estrellas=Integer.parseInt(request.getParameter("estrellas"));
             System.out.println("tamaño de estrellas"+estrellas);
        out.close();
     
} catch (Exception e) {
    System.out.println(""+e);
}
        
        
        
        
     
        }//cierre de for
       //mandar variables a modeloVisita
        ModeloVisita modelo= new ModeloVisita(nombre,fecha,costo,descripcion,estrellas,im2);
        //Crear objeto de BO
        BoRegVisita bo = new BoRegVisita();
             System.out.println(""+modelo);
        //mandar variables a metodo de reglas
        bo.reglas(modelo, out5,request);
       
       
    }
    
    
    
          private String getFileName(final Part part) {
    final String partHeader = part.getHeader("content-disposition");
    LOGGER.log(Level.INFO, "Part Header = {0}", partHeader);
    for (String content : part.getHeader("content-disposition").split(";")) {
        if (content.trim().startsWith("filename")) {
            return content.substring(
                    content.indexOf('=') + 1).trim().replace("\"", "");
        }
    }
    return null;
}
    
    
    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
