
package controlador.fernanda;

import bo.fernanda.BoEditarDatos;
import conexion.Conexion;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.System.out;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.fernanda.ModeloUsuario;

/**
 *
 * @author cuetl
 */
@WebServlet(name = "servEditarDatos", urlPatterns = {"/servEditarDatos"})
public class ControladorEditarDatos extends HttpServlet {
      ResultSet cdr=null;
    Statement sentenciaSQL=null;
    Conexion conecta=new Conexion();
    
      public void init (ServletConfig config) throws
            ServletException{
        super.init(config);
        conecta.Conectar();
        sentenciaSQL=conecta.getSentenciaSQL();
      }


    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet servEditarDatos</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet servEditarDatos at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
             throws ServletException, IOException {
          response.setContentType("text/html");
        PrintWriter out = response.getWriter();   
      //declaracion de variables
String nombre = request.getParameter("nombre");
String paterno= request.getParameter("paterno");
String contrasena= request.getParameter("contrasena");
//creacion de objeto de tipo usuario para mandarle nombre,apellido y contraseña
ModeloUsuario model= new ModeloUsuario();
model.setNombre(nombre);
model.setPaterno(paterno);
model.setContrasena(contrasena);
//creacion del objedto de tipo BoEditarDatos
BoEditarDatos bo= new BoEditarDatos();
bo.reglas(model,out,request);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
