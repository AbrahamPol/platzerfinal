
package controlador.fernanda;

import conexion.Conexion;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ServRegVisita2", urlPatterns = {"/ServRegVisita2"})
public class ServRegVisita2 extends HttpServlet {
     
    ResultSet cdr = null;
    Statement sentenciaSQL = null;
    Conexion conecta = new Conexion();
    
    public void init(ServletConfig config) throws ServletException{
       super.init(config);
       conecta.Conectar();
       sentenciaSQL = conecta.getSentenciaSQL();
    }

  
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServRegVisita2</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ServRegVisita2 at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html:charset=UTF-8");
        PrintWriter out = response.getWriter();
        String nombre = request.getParameter("nombre");
        System.out.println(nombre);
        try {
            cdr = sentenciaSQL.executeQuery("SELECT * FROM LUGAR WHERE NOMBRE = '"+nombre+"' ");
           
                 
            while(cdr.next()){
                out.print("<form enctype='multipart/form-data' id='formuVisita'>");
          
                out.println("<center>"
                + "<div class=login-wrap id=regform>"
                + "<h3>Registra visita</h3>"
                + "<input type=text value='"+cdr.getString("NOMBRE")+"' class=form-control id=nombrev name=nombrev placeholder=Nombre* readonly=readonly pattern=[^0-9] title=\"Sin números perro\"><br/><br/>"
                +"<img src="+cdr.getString("FOTO")+" class='img-responsive img-circle' alt='User'><br/><br/>"
                + "<input type=date class=form-control id=fechalugar name=fechalugar><br/><br/>"
                + "<input type=text class=form-control id=costolugar name=costolugar placeholder=Costo  onkeypress=\"return soloNumeros(event);\" ><br/><br/>"
       + "<input type=text class=form-control id=descripcionlugar name=descripcionlugar placeholder=descripcion><br/><br/>"
         + "<input type=file name=imagenVisitaFer id=imagenVisitaFer ><br/><br/>"
       
                + "</select><br/><br/>"
                        +"  <p class=\"clasificacion\">\n" +
"    <input id=\"radio1\" type=\"radio\" name=\"estrellas\" value=\"5\"><!--\n" +
"    --><label for=\"radio1\">&#9733;</label><!--\n" +
"    --><input id=\"radio2\" type=\"radio\" name=\"estrellas\" value=\"4\"><!--\n" +
"    --><label for=\"radio2\">&#9733;</label><!--\n" +
"    --><input id=\"radio3\" type=\"radio\" name=\"estrellas\" value=\"3\"><!--\n" +
"    --><label for=\"radio3\">&#9733;</label><!--\n" +
"    --><input id=\"radio4\" type=\"radio\" name=\"estrellas\" value=\"2\"><!--\n" +
"    --><label for=\"radio4\">&#9733;</label><!--\n" +
"    --><input id=\"radio5\" type=\"radio\" name=\"estrellas\" value=\"1\"><!--\n" +
"    --><label for=\"radio5\">&#9733;</label>\n" +
"  </p>"
                     +   "<input type=button class=btn btn-theme btn-block value=Registrar onclick=RegVist();> </form>"
                                                                       
                + "</div>"
                + "<div id=actVisit></div>"   
                     
                + "</center>");
                  
          
            }
             
        } catch (SQLException ex) {
            ex.getMessage();
            ex.printStackTrace();
        }
    }

    

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
