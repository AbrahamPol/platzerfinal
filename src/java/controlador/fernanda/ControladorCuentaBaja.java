
package controlador.fernanda;

import bo.fernanda.BoEditarDatos;
import conexion.Conexion;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.fernanda.ModeloUsuario;

/**
 *
 * @author Abraham
 */
@WebServlet(name = "ControladorCuentaBaja", urlPatterns = {"/ControladorCuentaBaja"})
public class ControladorCuentaBaja extends HttpServlet {
 ResultSet cdr = null;
    Statement sentenciaSQL = null;
    Conexion conecta = new Conexion();
    
    public void init(ServletConfig config) throws ServletException{
       super.init(config);
       conecta.Conectar();
       sentenciaSQL = conecta.getSentenciaSQL();
    }
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ControladorCuentaBaja</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ControladorCuentaBaja at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       response.setContentType("text/html");
        PrintWriter out = response.getWriter();  
         //aqui no se implementa mvc 4 por que solo es cambiar un estatus a la base de datos

try {
//obtener id de session
HttpSession usuario = request.getSession();
final String sql="update usuario set estado='BAJA' where IDUSUARIO='"+usuario.getAttribute("id_usuario")+"'";
sentenciaSQL.executeUpdate(sql);

out.print("index.jsp");
} catch (Exception e) {
System.out.println("error de baja"+e);
}finally{
    conecta.cerrar();
}
    }

   
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
