
package controlador.fernanda;

import bo.fernanda.BOActualizarViaje;
import bo.fernanda.BOEliminarViaje;
import bo.fernanda.BoRegistrarViaje;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.fernanda.ModeloViaje;

/**
 *
 * @author Abraham
 */
@WebServlet(name = "ControladorActualizarViaje", urlPatterns = {"/ControladorActualizarViaje"})
public class ControladorActualizarViaje extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ControladorActualizarViaje</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ControladorActualizarViaje at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         response.setContentType("text/html;charset=UTF-8");
       PrintWriter out= response.getWriter();
        try {
          String id_lugar=request.getParameter("id_viaje");
    //tabla
    out.println("<center><h1>Actualizar fecha de viaje</h1><br><br>");
    out.println("Fecha: <input type=date id=fechaactualizarviaje><br><br>");
    out.println("<input type=button value=Actualizar onclick=actualizarviaje2('"+id_lugar+"');><br><br>");
    out.println("</center>");
    //cabeceras
        } catch (Exception e) {
            System.out.println("error controlador"+e);
        }

    out.close();
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
 try {
//metodo session
int id_viaje=Integer.parseInt(request.getParameter("id_viaje"));
String fecha=request.getParameter("fechaactualizarviaje");
//se crea un objeto de DAO
ModeloViaje modelo= new ModeloViaje();
modelo.setId_viaje(id_viaje);
modelo.setFecha(fecha);
     System.out.println(""+modelo);
BOActualizarViaje bo = new BOActualizarViaje();
bo.reglas(modelo);
} catch (Exception e) {
System.out.println("error de controlador"+e);
}
    }

    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
