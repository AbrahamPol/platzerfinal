
package controlador.fernanda;

import bo.fernanda.BoCambiarFoto;
import bo.fernanda.BoRegLugar;
import conexion.Conexion;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import modelo.fernanda.ModeloLugar;
import modelo.fernanda.ModeloUsuario;

/**
 *
 * @author cuetl
 */
@WebServlet(name = "ServRegLugar", urlPatterns = {"/ServRegLugar"})
@MultipartConfig
public class ControladorRegLugar extends HttpServlet {
        
    private final static Logger LOGGER =  Logger.getLogger(ControladorRegLugar.class.getCanonicalName());
    public ControladorRegLugar(){
        super();
    }


    
    ResultSet cdr = null;
    Statement sentenciaSQL = null;
    Conexion conecta = new Conexion();
    
    public void init(ServletConfig config) throws ServletException{
       super.init(config);
       conecta.Conectar();
       sentenciaSQL = conecta.getSentenciaSQL();
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServRegLugar</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ServRegLugar at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         PrintWriter out = response.getWriter();
        response.setContentType("text/html:charset=UTF-8");
        out.println("<center>"
                + "<div class=login-wrap id=regform>"
                + "<h3>Registrar un Lugar</h3>"
                + "<form enctype='multipart/form-data' id='formuLugar'>"
                + "<input type=text class=form-control name=nombre id=nombre placeholder=Nombre pattern=[^0-9]  onkeypress=\"return soloLetras(event);\"><br/><br/>"
               
                + "<input type=text class=form-control name=descripcionLugar id=descripcionLugar placeholder=Descripcion><br/><br/>"
                +" <input class=input type='file'  placeholder='Imagen' name='imagenLugar' id='imagenLugar'><br><br>"
                + "<input type=button  class=\"btn btn-theme btn-block\" value=\"Registrar\" onclick=registrarLugar();><br/><br/>"                                                        
                + "</div>"
                + "<div id=registroya></div>"                
                + "</center>"
        +"</form>");
      
   

    }

 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        for (Part part : request.getParts()) {//for para recorrer el tamaño del archivo
         try{
               
PrintWriter out5 = response.getWriter(); 
// IMPORTANTE DECLARAR ESTAS CONSTANTES
//EL STRING PATH DEBE IR CON LA RUTA DE TU PC
//request.getPart(aqui va el nombre que le envia ajax en el post ID)
//final String path ="C:/Users/Abraham/Documents/ABRAHAM/Integradora fer/Platzer/web/imgFer/";
//aqui cambia tu ruta y comenta la linea anterior
final String path = "C:/Users/Abraham/Documents/ABRAHAM/integradora final/platzer/web/imgFer/";

//C:\Users\Abraham\Documents\ABRAHAM\integradora final\platzer\web\imgFer
final Part filePart = request.getPart("imagenLugar");
final String fileName = getFileName(filePart);
//OutPutStream y Stream son necesarias para hacer el proceso de datos
OutputStream out = null;
InputStream filecontent = null;
//PrintWrite para sobreescribir el path del archivo
final PrintWriter writer = response.getWriter();
String im="",im2="";
im=request.getParameter("imagenLugar");//se recibe el nombre del archivo del formulario


///////////////////////////////CODIGO PARA GUARDAR LA IMAGEN///////////////////////////////////////////////7
 try {
out = new FileOutputStream(new File(path + File.separator
+ fileName));
filecontent = filePart.getInputStream();
int read = 0;
final byte[] bytes = new byte[1024];
while ((read = filecontent.read(bytes)) != -1) {
out.write(bytes, 0, read);
}
im2="../imgFer/"+fileName;
    } catch (FileNotFoundException fne) {}


   ///////////////////////////////////////////////////////////////////7
        //PrintWriter out = response.getWriter();
        response.setContentType("text/html:charset=UTF-8");
        //recibimiento de parametros
        String nombre = request.getParameter("nombre").toUpperCase();
        String descripcion= request.getParameter("descripcionLugar").toUpperCase();
        //crear objeto de modelo
        ModeloLugar modelo= new ModeloLugar(nombre,descripcion,im2);
        //crear un objeto de BO
        BoRegLugar bo= new BoRegLugar();
        //mandar el modelo y out a al metodo reglas
        bo.reglas(modelo,out5);

} catch (Exception e) {
    System.out.println(""+e);
}
        
        
        
        
     
        }//cierre de for
    }//servlet cierre

        private String getFileName(final Part part) {
    final String partHeader = part.getHeader("content-disposition");
    LOGGER.log(Level.INFO, "Part Header = {0}", partHeader);
    for (String content : part.getHeader("content-disposition").split(";")) {
        if (content.trim().startsWith("filename")) {
            return content.substring(
                    content.indexOf('=') + 1).trim().replace("\"", "");
        }
    }
    return null;
}
  
    

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
