
package controlador.fernanda;

import conexion.Conexion;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author cuetl
 */
@WebServlet(name = "ServMostVisita", urlPatterns = {"/ServMostVisita"})
public class ControladorMostVisita extends HttpServlet {
  ResultSet cdr = null;
    Statement sentenciaSQL = null;
    Conexion conecta = new Conexion();
    
    public void init(ServletConfig config) throws ServletException{
       super.init(config);
       conecta.Conectar();
       sentenciaSQL = conecta.getSentenciaSQL();
    }
  
  
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServMostVisita</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ServMostVisita at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
 response.setContentType("text/html:charset=UTF-8");
        PrintWriter out = response.getWriter(); 
        
        out.println(" <section class=\"single-post\">"+
                " <div class=\"container\">");
        out.println("<div class=\"row\">");
        out.println("<div class=\"modal-dialog\">");
        out.println("<div class=\"modal-content\">");
        out.println("<div class=\"modal-body\">");
        out.println(" <div class=\"row\">");
        
          int c = 1;
        try {            
            cdr = sentenciaSQL.executeQuery("SELECT * FROM VISITA ");            
            while(cdr.next()){
                out.println(" <div class=\"col-md-8 modal-image\">"+
                        " <img class=\"img-responsive\" src=\"assets/img/posts/15.jpg\" alt=\"Image\"/>"+
                        
                        " <div class=\"col-md-4 modal-meta\">"+
                        " <div class=\"modal-meta-top\">"+
                        ""+
                        ""+
                        ""+
                        ""+
                        ""+
                        ""+
                        ""+
                        "");
                
                      }
        out.write("</div>"
                + "</div>"
                + "</section>");
        } catch (SQLException ex) {
            ex.getMessage();
            ex.printStackTrace();
        }
            }
    

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

   
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
