
package controlador.fernanda;

import conexion.Conexion;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Abraham
 */
@WebServlet(name = "ControladorPublicacionesFavoritas", urlPatterns = {"/ControladorPublicacionesFavoritas"})
public class ControladorPublicacionesFavoritas extends HttpServlet {

        ResultSet cdr = null;
    Statement sentenciaSQL = null;
    Conexion conecta = new Conexion();
    
        ResultSet cdr2 = null;
    Statement sentenciaSQL2 = null;
    Conexion conecta2 = new Conexion();
    
     ResultSet cdr3 = null;
    Statement sentenciaSQL3 = null;
    Conexion conecta3 = new Conexion();
    
    public void init(ServletConfig config) throws ServletException{
       super.init(config);
       conecta.Conectar();
       sentenciaSQL = conecta.getSentenciaSQL();
        conecta2.Conectar();
       sentenciaSQL2 = conecta2.getSentenciaSQL();
       conecta3.Conectar();
       sentenciaSQL3 = conecta3.getSentenciaSQL();
    }
    
  
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ControladorPublicacionesFavoritas</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ControladorPublicacionesFavoritas at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

  
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
      response.setContentType("text/html");
        PrintWriter out = response.getWriter();  
        
        try {
 /////////////////////////////PARTE DE LAS PUBLICACIONES///////////////////////////////////////////           
HttpSession usuario = request.getSession();
final  String sql="select vi.idvisita,lu.nombre,vi.imagen,vi.fecha,vi.costo,vi.DESCRIPCION,vi.CALIFICACION,pu.IDPUBLICACION\n" +
"from visita vi\n" +
"inner join publicacion pu\n" +
"on vi.IDVISITA=pu.IDVISITA\n" +
"inner join lugar lu\n" +
"on lu.IDLUGAR=vi.IDLUGAR\n" +
"inner join VISITAFAVORITA vf\n" +
"on vf.IDVISITA=vi.IDVISITA\n"+
"where vi.IDUSUARIO="+usuario.getAttribute("id_usuario")+" ";
cdr=sentenciaSQL.executeQuery(sql);
        while (cdr.next()) {
        final String id_P=cdr.getString("IDPUBLICACION");
        final String id_visita=cdr.getString("IDVISITA");
        out.print("<section class=\"single-post\">");
        out.print("<div class=\"container\">");
        out.print("<div class=\"modal-dialog\">");
        out.print("<div class=\"modal-dialog\">");
        out.print("<div class=\"modal-content\">");
        out.print("<div class=\"modal-body\">");
        out.print(" <div class=\"row\">");
        out.print("<div class=\"col-md-8 modal-image\">");
        //FOTO LUGAR
        out.print("<img class=\"img-responsive\" src="+cdr.getString("imagen")+" alt=\"Image\"/>");
        out.print("</div><!--/ col-md-8 -->");
        out.print("<div class=\"col-md-4 modal-meta\">");
        out.print("<div class=\"modal-meta-top\">");
        out.print("<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">");
        out.print("<span aria-hidden=\"true\">×</span><span class=\"sr-only\">Close</span>");
        out.print("</button><!--/ button -->");
        out.print("<div class=\"img-poster clearfix\">");
        //hacer una consulta con el id de usuario para saber su foto
        final String sql2="select * from usuario where IDUSUARIO="+usuario.getAttribute("id_usuario")+"";
        cdr2=sentenciaSQL2.executeQuery(sql2);
        while (cdr2.next()){
            out.print("<a href=\"\"><img class=\"img-responsive img-circle\" src="+cdr2.getString("foto")+" alt=\"Image\"/></a>");
         out.print("<strong><a href=\"\">"+cdr2.getString("nombre")+" "+"</a></strong>");
        }//while usuario
        cdr2.close();
        out.print("<span>el  "+" "+cdr.getString("fecha")+"</span>"); 
///VALIDACION PARA SABER SI LO AÑADIO A FAVORITOS

final  String sql4="select * from visitafavorita where idvisita="+id_visita+"";
cdr3=sentenciaSQL3.executeQuery(sql4);
if(cdr3.next()){
         out.print("<input type=hidden value="+id_visita+" id=id_visitaFer2>");
    out.print("<button class=\"btn btn-default btn-theme\" onclick=\"peticionControladorEliminarVisitaFavorita();\">Eliminar de mis favoritos</button>");

}

///////////////////7  
        out.print("</div><!--/ img-poster -->");
        out.print("<ul class=\"img-comment-list\">");
        out.print("<li>");
        out.print("<div class=\"comment-img\">");
        //comentarios
        final String sql3="select co.COMENTARIO,co.fecha,co.HORA,us.foto,CONCAT(us.nombre,' ',us.APATERNO) as nombre\n" +
"from COMENTARIO co\n" +
"inner join publicacion pu\n" +
"on pu.IDPUBLICACION=co.IDPUBLICACION\n" +
"inner join visita vi\n" +
"on vi.IDVISITA=pu.IDVISITA\n" +
"inner join USUARIO us\n" +
"on us.IDUSUARIO=vi.IDUSUARIO\n" +
"where co.IDPUBLICACION='"+id_P+"'";
        cdr2=sentenciaSQL2.executeQuery(sql3);
while(cdr2.next()){
out.print("<img src="+cdr2.getString("foto")+" class=\"img-responsive img-circle\" alt=\"Image\"/>");
out.print("</div>");
out.print("<div class=\"comment-text\">");  
out.print("<strong><a href=\"\">"+cdr2.getString("nombre")+"</a></strong>");
out.print("<p>"+cdr2.getString("COMENTARIO")+"</p> <span class=\"date sub-text\">"+cdr2.getString("fecha")+"</span>");
out.print("</div>");
out.print("</li><!--/ li -->");
out.print("<li>");
out.print("<div class=\"comment-img\">");
        }//while comentarios


     
    
        out.print("</ul><!--/ comment-list -->");
        out.print("<div class=\"modal-meta-bottom\">");
        out.print("<ul>");
        out.print("<li><a class=\"modal-like\" href=\"#\"><i class=\"fa fa-heart\"></i></a><span class=\"modal-one\"> </span> | ");
        out.print("<a class=\"modal-comment\" href=\"#\"><i class=\"fa fa-comments\"></i></a><span> </span> </li>");
        out.print("<li>");
        out.print("<span class=\"thumb-xs\">");
        out.print("</span>");
        out.print("<div class=\"comment-body\">");
        out.print("</div><!--/ comment-body -->	");
        out.print("</li>");
        out.print("</ul>");
        out.print("</div><!--/ modal-meta-bottom -->");
        out.print("</div><!--/ modal-meta-top -->");
        out.print("</div><!--/ col-md-4 -->");
        out.print("</div><!--/ row -->");
        out.print("</div><!--/ modal-body -->");
        out.print("</div><!--/ modal-content -->");
        out.print("</div><!--/ modal-dialog -->");
        out.print("</div><!--/ modal-dialog -->");
        out.print("</section><!--/ modal -->");
     
    
            }//while
        } catch (Exception e) {
            System.out.println(""+e);
        }
       
      out.close();
        
    }

   
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
