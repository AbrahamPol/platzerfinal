
package controlador.fernanda;

import conexion.Conexion;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Abraham
 */
@WebServlet(name = "ControladorInicioViajes", urlPatterns = {"/ControladorInicioViajes"})
public class ControladorInicioViajes extends HttpServlet {

 
    ResultSet cdr=null;
    Statement sentenciaSQL=null;
    Conexion conecta= new Conexion();
    @Override
    public void init(ServletConfig config) throws ServletException {
      super.init(config);
      conecta.Conectar();
      sentenciaSQL=conecta.getSentenciaSQL();
    }
 
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ControladorInicioViajes</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ControladorInicioViajes at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
       PrintWriter out= response.getWriter();
        try {
           String strComando="select (select nombre from lugar)as nombre,fecha,idviaje from viaje";
            cdr=sentenciaSQL.executeQuery(strComando); 
                      //contenido
    ////////////////////////77
    //tabla
    
         out.println("<table width=100% border=1>");
    //cabeceras
    out.println("<tr>");
    out.println("<th>Nombre</th><th>Fecha</th><th></th><th></th>");
    out.println("</tr>"); 
   
  
      //impresion de filas
     while (cdr.next()) {
    out.println("<tr>");
    out.println("<th width=10%>"+cdr.getString("nombre")+"</th>");
    out.println("<th width=30%>"+cdr.getString("fecha")+"</th>");
    out.println("<th width=30%><input type=button  value=Eliminar onclick=eliminarViaje('"+cdr.getString("idviaje")+"');></th>");
 out.println("<th width=30%><input type=button  value=Actualizar onclick=actualizarViaje('"+cdr.getString("idviaje")+"');></th>");
    out.println("</tr>");
     }
     /////////////////////  
    out.println("</table> <br>");
        } catch (Exception e) {
            System.out.println("error controlador"+e);
        }

    out.close();
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
    }

  
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
