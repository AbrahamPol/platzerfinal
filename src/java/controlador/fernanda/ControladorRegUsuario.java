
package controlador.fernanda;
import bo.fernanda.BoUsuario;
import modelo.fernanda.*;
import conexion.Conexion;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Hernandez
 */
@WebServlet(name = "ServRegUsuario", urlPatterns = {"/ServRegUsuario"})
public class ControladorRegUsuario extends HttpServlet {

    ResultSet cdr = null;
    Statement sentenciaSQL = null;
    Conexion conecta = new Conexion();
    
    public void init(ServletConfig config) throws ServletException{
       super.init(config);
       conecta.Conectar();
       sentenciaSQL = conecta.getSentenciaSQL();
    }
 
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServRegUsuario</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ServRegUsuario at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
      PrintWriter out = response.getWriter(); 
        response.setContentType("text/html:charset=UTF-8");
      
        try {
            //recibo de datos
        String email = request.getParameter("emailu");
        String password = request.getParameter("passwordu");
        String nombre = request.getParameter("nombreu");
        String apaterno = request.getParameter("apaterno"); 
        //crear objeto de usuario para manipularlo en la bo
        ModeloUsuario usuario= new ModeloUsuario(email,password,nombre,apaterno);
    
        //se crea un objeto de la clase BoUsuario
        BoUsuario bo= new BoUsuario();
        //el metodo de la clase bo recibe el objeto de usuario
        //y out, para imprimir mensajes en alerts
        bo.reglas(usuario, out);
        //out.close();
        } catch (Exception ex) {
            System.out.println(""+ex);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
