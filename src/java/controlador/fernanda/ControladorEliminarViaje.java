
package controlador.fernanda;

import bo.fernanda.BOEliminarViaje;
import bo.fernanda.BoRegistrarViaje;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.fernanda.ModeloViaje;

/**
 *
 * @author Abraham
 */
@WebServlet(name = "ControladorEliminarViaje", urlPatterns = {"/ControladorEliminarViaje"})
public class ControladorEliminarViaje extends HttpServlet {

  
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ControladorEliminarViaje</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ControladorEliminarViaje at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
throws ServletException, IOException {
try {
//metodo session
int id_viaje=Integer.parseInt(request.getParameter("id_viaje"));
//se crea un objeto de DAO
ModeloViaje modelo= new ModeloViaje();
modelo.setId_viaje(id_viaje);
BOEliminarViaje bo= new BOEliminarViaje();
bo.reglas(modelo);
} catch (Exception e) {
System.out.println("error de controlador"+e);
}
    }

   
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
