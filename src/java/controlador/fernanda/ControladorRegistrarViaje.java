
package controlador.fernanda;

import bo.fernanda.BoRegistrarViaje;
import conexion.Conexion;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.fernanda.ModeloViaje;

/**
 *
 * @author Abraham
 */
@WebServlet(name = "ControladorRegistrarViaje", urlPatterns = {"/ControladorRegistrarViaje"})
public class ControladorRegistrarViaje extends HttpServlet {

     ResultSet cdr=null;
    Statement sentenciaSQL=null;
    Conexion conecta= new Conexion();
    @Override
    public void init(ServletConfig config) throws ServletException {
      super.init(config);
      conecta.Conectar();
      sentenciaSQL=conecta.getSentenciaSQL();
    }
 
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ControladorRegistrarViaje</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ControladorRegistrarViaje at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       response.setContentType("text/html;charset=UTF-8");
       PrintWriter out= response.getWriter();
try {
//tabla
out.println("<center><h2>Registro de tu proximo viaje</h2><br><br>");
out.println("Nombre del lugar <select id=nombreLugarViaje><br><br>");
out.print("<option>Elige</option>");
cdr=sentenciaSQL.executeQuery("select nombre from lugar");
while(cdr.next()){
out.print("<option value="+cdr.getString(1)+">"+cdr.getString(1)+"</option>");
}
out.println("</select><br><br>");
out.println("Fecha: <input type=date id=fechaviaje><br><br>");
out.println("<input type=button value=Registrar onclick=registrarViaje();><br><br>");
out.println("</center>");
//cabeceras
} catch (Exception e) {
System.out.println("error controlador"+e);
}

    out.close();
    }

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
try {
//metodo session
HttpSession usuario = request.getSession();
int id_usuario=Integer.parseInt((String) usuario.getAttribute("id_usuario"));
String fecha=request.getParameter("fechaviaje");
String nombreLugar=request.getParameter("nombreLugarViaje");
//se crea un objeto de DAO
ModeloViaje modelo= new ModeloViaje(id_usuario,fecha,nombreLugar);
    BoRegistrarViaje bo= new BoRegistrarViaje();
    bo.reglas(modelo);
} catch (Exception e) {
System.out.println("error de controlador"+e);
}
    }

   
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
