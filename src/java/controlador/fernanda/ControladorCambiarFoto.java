
package controlador.fernanda;

import bo.fernanda.BoCambiarFoto;
import conexion.Conexion;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.util.logging.Level;
import javax.servlet.ServletConfig;
import javax.servlet.http.HttpSession;
import modelo.fernanda.ModeloUsuario;

@WebServlet(name = "ControladorCambiarFoto", urlPatterns = {"/ControladorCambiarFoto"})
@MultipartConfig
public class ControladorCambiarFoto extends HttpServlet {

     private final static Logger LOGGER =  Logger.getLogger(ControladorCambiarFoto.class.getCanonicalName());
    public ControladorCambiarFoto(){
        super();
    }


    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ControladorCambiarFoto</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ControladorCambiarFoto at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

  
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
             for (Part part : request.getParts()) {//for para recorrer el tamaño del archivo
     try {
PrintWriter out5 = response.getWriter(); 
// IMPORTANTE DECLARAR ESTAS CONSTANTES
//EL STRING PATH DEBE IR CON LA RUTA DE TU PC
//request.getPart(aqui va el nombre que le envia ajax en el post ID)
//final String path = "E:/JOSE/platzer/web/imgFer/";
final String path = "C:/Users/Abraham/Documents/ABRAHAM/integradora final/platzer/web/imgFer/";
//C:\Users\cuetl\Documents\2do cuatri\platzer\web\imgFer
//final String path ="C:/Users/Abraham/Documents/ABRAHAM/Integradora fer/Platzer/web/imgFer/";
final Part filePart = request.getPart("imagenPerfil");
final String fileName = getFileName(filePart);
//OutPutStream y Stream son necesarias para hacer el proceso de datos
OutputStream out = null;
InputStream filecontent = null;
//PrintWrite para sobreescribir el path del archivo
final PrintWriter writer = response.getWriter();
String im="",im2="";
im=request.getParameter("imagenPerfil");//se recibe el nombre del archivo del formulario


///////////////////////////////CODIGO PARA GUARDAR LA IMAGEN///////////////////////////////////////////////7
 try {
out = new FileOutputStream(new File(path + File.separator
+ fileName));
filecontent = filePart.getInputStream();
int read = 0;
final byte[] bytes = new byte[1024];
while ((read = filecontent.read(bytes)) != -1) {
out.write(bytes, 0, read);
}
im2="../imgFer/"+fileName;
    } catch (FileNotFoundException fne) {}
/*
 Despues creamos un objeto de la clase ModeloUsuario
 para ocupar los metodo de acceo de foto,id.
 */
ModeloUsuario modelo= new ModeloUsuario();
modelo.setFoto(im2);
//despues le mandamos el id del usuario
HttpSession usuario = request.getSession();
int id=Integer.parseInt((String) usuario.getAttribute("id_usuario"));
modelo.setId(id);
//Despues se crea un objeto de la clase BOFoto
BoCambiarFoto bo = new BoCambiarFoto();
//y el metodo reglas recibe el objeto de usuario
bo.reglas(modelo);


} catch (Exception e) {
    System.out.println(""+e);
}
     
    }
    }

     private String getFileName(final Part part) {
    final String partHeader = part.getHeader("content-disposition");
    LOGGER.log(Level.INFO, "Part Header = {0}", partHeader);
    for (String content : part.getHeader("content-disposition").split(";")) {
        if (content.trim().startsWith("filename")) {
            return content.substring(
                    content.indexOf('=') + 1).trim().replace("\"", "");
        }
    }
    return null;
}
    
    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
