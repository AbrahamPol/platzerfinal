
package controlador.fernanda;

import conexion.Conexion;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet(name = "ControladorInicio", urlPatterns = {"/ControladorInicio"})
public class ControladorInicio extends HttpServlet {
    
    ResultSet cdr = null;
    Statement sentenciaSQL = null;
    Conexion conecta = new Conexion();
    
        ResultSet cdr2 = null;
    Statement sentenciaSQL2 = null;
    Conexion conecta2 = new Conexion();
    
    public void init(ServletConfig config) throws ServletException{
       super.init(config);
       conecta.Conectar();
       sentenciaSQL = conecta.getSentenciaSQL();
        conecta2.Conectar();
       sentenciaSQL2 = conecta2.getSentenciaSQL();
    }
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ControladorInicio</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ControladorInicio at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();  
 
        try {
            ///CONSULTA
final String sql="select lu.nombre,vi.imagen,vi.fecha,vi.IDUSUARIO,vi.costo,vi.DESCRIPCION,vi.CALIFICACION,pu.IDPUBLICACION\n" +
"from visita vi\n" +
"inner join publicacion pu\n" +
"on vi.IDVISITA=pu.IDVISITA\n" +
"inner join lugar lu\n" +
"on lu.IDLUGAR=vi.IDLUGAR;";
cdr=sentenciaSQL.executeQuery(sql);
          

while (cdr.next()){
final String id_P=cdr.getString("IDPUBLICACION");
final String id_Visita=cdr.getString("IDUSUARIO");
////////////////////////////////////////////////////////77
out.print(" <section class=\"single-post\">");
out.print("<div class=\"container\">");
out.print("<div class=\"row\">");
out.print("<div class=\"\">");
out.print("<div class=\"modal-content\">");
out.print("<div class=\"modal-body\">");
out.print("<div class=\"row\">");
out.print("<div class=\"col-md-8 modal-image\">");
// FOTO DEL LUGAR 
out.print(""+cdr.getString("nombre")+" "+"  "+cdr.getString("descripcion")+" "+"   Calificacion: " +cdr.getString("calificacion")+" "+"  "+cdr.getString("fecha")+" "+"  Costo: "+cdr.getString("costo")+" "+""); 
out.print("<br><br>");
out.print("<img class=\"img-responsive\" src="+cdr.getString("imagen")+" alt=\"Image\"/>");  
out.print("</div><!--/ col-md-8 -->");
out.print("<div class=\"col-md-4 modal-meta\">");
out.print("<div class=\"modal-meta-top\">");
out.print("<div class=\"img-poster clearfix\">");

  //hacer una consulta con el id de usuario para saber su foto
final String sql3="select * from usuario where IDUSUARIO="+id_Visita+"";
cdr2=sentenciaSQL2.executeQuery(sql3);
while (cdr2.next()){
out.print("<a href=\"\"><img class=\"img-responsive img-circle\" src="+cdr2.getString("foto")+" alt=\"Image\"/></a>");
out.print("<strong><a href=\"\">"+cdr2.getString("nombre")+" "+"</a></strong>");
}//while usuario
cdr2.close();
out.print("<span>el  "+" "+cdr.getString("fecha")+"</span><br/>"); 

out.print("</div><!--/ img-poster -->");



//COMENTARIOS
out.print("<ul class=\"img-comment-list\">");
out.print("<div id=\"mostrarComentarios\" class=\"comment-text\">");
out.print("<li>");

 // COMENTARIOS
 String sql2="select co.COMENTARIO,co.fecha,co.HORA,us.foto,CONCAT(us.nombre,' ',us.APATERNO) as nombre\n" +
"from COMENTARIO co\n" +
"inner join publicacion pu\n" +
"on pu.IDPUBLICACION=co.IDPUBLICACION\n" +
"inner join visita vi\n" +
"on vi.IDVISITA=pu.IDVISITA\n" +
"inner join USUARIO us\n" +
"on us.IDUSUARIO=vi.IDUSUARIO\n" +
"where co.IDPUBLICACION='"+id_P+"'";
cdr2=sentenciaSQL2.executeQuery(sql2);
 while(cdr2.next()){
     
out.print("<div class=\"comment-img\">"); 
out.print("<img src="+cdr2.getString("foto")+" class=\"img-responsive img-circle\" alt=\"Image\"/>");
out.print("</div>");
out.print("<div class=\"comment-text\">");  
out.print("<strong><a href=\"\">"+cdr2.getString("nombre")+"</a></strong>");  
out.print("<p class=\"\">"+cdr2.getString("comentario")+"</p> <span class=\"date sub-text\">"+cdr2.getString("fecha")+"   "+" "+cdr2.getString("hora")+" </span>");  
 out.print("</div>");
 }
cdr2.close();
out.print("</li>"); 
 /////////////////////////////////////////////////////////////////7
//
//<!-- ==============================================
//Scripts
//=============================================== -->
out.print("<script src=\"assets/js/jquery.min.js\"></script>");  
out.print("<script src=\"assets/js/bootstrap.min.js\"></script>");
out.print("<script src=\"assets/js/base.js\"></script>");
out.print("<script src=\"assets/plugins/slimscroll/jquery.slimscroll.js\"></script>");
out.print("<script>");  
out.print("$('#Slim,#Slim2').slimScroll({");
out.print("height:\"auto\",");
out.print("position: 'right',");
out.print("railVisible: true,");  
out.print("alwaysVisible: true,");
out.print("size:\"8px\",");
out.print("});	");
out.print("</script>");  
out.print("</div>");
// Espacio para Publicaciones 
out.print("</ul><!--/ comment-list -->    ");
out.print("<div class=\"modal-meta-bottom\">");
out.print("<ul>");
out.print("<li><a class=\"modal-like\" href=\"#\"><i class=\"fa fa-heart\"></i></a><span class=\"modal-one\"> </span> | ");
out.print("<a class=\"modal-comment\" href=\"#\"><i class=\"fa fa-comments\"></i></a><span></span> </li>");
out.print("<li>");
out.print("<span class=\"thumb-xs\">");
//imagen de la persona de inicio de session
HttpSession usuario = request.getSession();
String sql5="select * from usuario where IDUSUARIO="+usuario.getAttribute("id_usuario")+"";
cdr2=sentenciaSQL2.executeQuery(sql5);
while(cdr2.next()){
   out.print("<img class=\"img-responsive img-circle\" src="+cdr2.getString("foto")+" alt=\"Image\">");
 
}//while de perfil
cdr2.close();
//
out.print("</span>");
out.print("<div class=\"comment-body\">");
out.print("<input class=\"form-control input-sm\" type=\"text\" placeholder=\"Escribe tu Comentario...\"><br/>");
out.print("<button class=\"btn btn-default btn-theme\" onclick=\"comentarPublicacion(); mostrarComentarios();\"><span>Comentar</span></button>");
out.print("</div><!--/ comment-body -->");
out.print("</li>");
out.print("</ul>");
out.print("</div><!--/ modal-meta-bottom -->");
out.print("</div><!--/ modal-meta-top -->");
out.print("</div><!--/ col-md-4 -->");
out.print("</div><!--/ row -->");
out.print("</div><!--/ modal-body -->");
out.print("</div><!--/ modal-content -->");              
out.print("</div><!--/ modal-dialog -->");
out.print("</div><!--/ modal-dialog -->");
out.print("</div><!--/ modal-dialog -->");
out.print("</section><!--/ modal -->");
//<!-- ==============================================
//Scripts
//=============================================== -->
out.print("<script src=\"assets/js/jquery.min.js\"></script>");
out.print("<script src=\"assets/js/bootstrap.min.js\"></script>");
out.print("<script src=\"assets/js/base.js\"></script>");
out.print("<script src=\"assets/plugins/slimscroll/jquery.slimscroll.js\"></script>");
out.print("<script>");
out.print("$('#Slim,#Slim2').slimScroll({");
out.print("height:\"auto\",");
out.print("position: 'right',");
out.print("railVisible: true,");   
out.print("alwaysVisible: true,");
out.print("size:\"8px\",");
out.print("});");
out.print("</script>");
out.print("<div >");
out.print("<input type=hidden id=nombreComentario value=<% out.println(usuario.getAttribute(nombre));%>"); 
out.print("<input type=hidden id=apellidoComentario value=<%out.println(usuario.getAttribute(apellido));%> ");
out.print("</div>");
out.print("</div>");

 }//cierre de while



out.close();

} catch (Exception e) {
            System.out.println("error de controlador INICIO"+e);
        }
    }

   
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
