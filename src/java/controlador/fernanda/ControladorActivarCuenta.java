
package controlador.fernanda;

import bo.fernanda.BoActivacion;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.fernanda.ModeloActivacion;

/**
 *
 * @author Abraham
 */
@WebServlet(name = "ServActivarCuenta", urlPatterns = {"/ServActivarCuenta"})
public class ControladorActivarCuenta extends HttpServlet {

   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServActivarCuenta</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ServActivarCuenta at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
           String codigo= request.getParameter("codigo");
           String id= request.getParameter("cl");
           ModeloActivacion activacion= new ModeloActivacion(id,codigo);
           BoActivacion ba=new BoActivacion();
           ba.reglas(activacion);
    /*
    despues de activar al usuario
    se crea un objeto de tipo session para ocupar ese id , para gestiones,altas publicaciones etc.
    y lo redireccionamos  a la pagina principal
           */  
    HttpSession usuario = request.getSession();
     usuario.setAttribute("id_usuario", activacion.getId_usuario());
           
      response.sendRedirect("http://localhost:8080/Fluffs/Platzer.jsp");
        } catch (Exception e) {
            System.out.println(""+e);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
