/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jose;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.Barcode;
import com.itextpdf.text.pdf.Barcode128;
import com.itextpdf.text.pdf.Barcode39;
import com.itextpdf.text.pdf.BarcodeEAN;
import com.itextpdf.text.pdf.BarcodeQRCode;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfWriter;
import com.sun.org.apache.xml.internal.serializer.ElemDesc;
import conexion.Conexion;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 *
 * @author Hernandez
 */
public class LineaCaptura {
    
    ResultSet cdr = null;
    Statement sentenciaSQL = null;
    Conexion conecta = new Conexion();
    
    int udm=0,resol=72,rot=0;
    float mi=0.000f, md=0.000f, ms=0.000f,min=0.000f,tam=5.00f;
    
    private Font fuenteBold = new Font(Font.FontFamily.COURIER, 14, Font.BOLD);    
    private Font fuenteNormal = new Font(Font.FontFamily.COURIER, 10, Font.NORMAL);    
    private Font fuenteItalic = new Font(Font.FontFamily.COURIER, 14, Font.ITALIC);

    public void generarPDF(String header,String info,String footer, String rutaImagen, String salida,String codigo,String nomEmpresa,String nomTour,double costo,String descripcion,String fecha,int cantidad){
        try {
            Document document = new Document(PageSize.A7,36,36,10,10);
            PdfWriter pw = PdfWriter.getInstance(document, new FileOutputStream(salida));
            document.open();
            document.add(getHeader(header));
            Image imagen = Image.getInstance(rutaImagen);
            imagen.scaleAbsolute(100, 75);
            imagen.setAlignment(Element.ALIGN_CENTER);
            document.add(imagen);
            document.add(getInfo(info));
            document.add(getInfo(""));
            document.add(getInfo(""));
            document.add(getInfo(""));
            document.add(getBarcode(document, pw, codigo));
            
            document.add(getInfo(""));
            document.add(getFooter(footer));
            document.add(getInfo(""));
            
            document.add(getInfo(""));
            Date date = new Date();
            DateFormat hourdateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            BarcodeQRCode barcodeQRCode = new BarcodeQRCode("La empresa "+nomEmpresa+" agradece su compra del tour "+nomTour+".\n"
                    + "Detalles...\n"
                    + "Costo: $"+costo+"\n"
                    + "Descripción: "+descripcion+"\n"
                    + "Cantidad de Personas: "+cantidad+"\n"
                    + "Fecha de Salida: "+fecha+"\n"
                    + "Fecha y Hora de Compra: "+hourdateFormat.format(date), 1000, 1000, null);            
            Image codeQrImage = barcodeQRCode.getImage();
            codeQrImage.scaleAbsolute(25, 25);
            document.add(codeQrImage);
            document.close();
        } catch (Exception e) {
            e.getMessage();
            e.printStackTrace();
        }
    }
    
    public Paragraph getHeader(String texto){
        Paragraph p = new Paragraph();
        Chunk c = new Chunk();
        p.setAlignment(Element.ALIGN_CENTER);
        c.append(texto);
        c.setFont(fuenteBold);
        p.add(c);
        return p;
    }
    
    public Paragraph getInfo(String texto){
        Paragraph p = new Paragraph();
        Chunk c = new Chunk();
        p.setAlignment(Element.ALIGN_JUSTIFIED_ALL);
        c.append(texto);
        c.setFont(fuenteNormal);
        p.add(c);
        return p;
    }
    
    public Paragraph getFooter(String texto){
        Paragraph p = new Paragraph();
        Chunk c = new Chunk();
        p.setAlignment(Element.ALIGN_CENTER);
        c.append(texto);
        c.setFont(fuenteItalic);
        p.add(c);
        return p;
    }
    
    private Image getBarcode(Document document, PdfWriter pw,String codigo){
        PdfContentByte cimg = pw.getDirectContent();
        Barcode128 code128 = new Barcode128();
        code128.setCode(formatearCodigo(codigo));
        code128.setCodeType(Barcode128.CODE128);
        code128.setTextAlignment(Element.ALIGN_CENTER);
        
        Image image = code128.createImageWithBarcode(cimg, BaseColor.BLACK, BaseColor.BLACK);
        float scaler = ((document.getPageSize().getWidth() - document.leftMargin() - document.rightMargin() - 0) / image.getWidth() * 60);
        image.scalePercent(scaler);
        image.setAlignment(Element.ALIGN_CENTER);
        return image;
    }
    
    private String formatearCodigo(String num){
        NumberFormat form = new DecimalFormat("0000000");
        return form.format((num != null) ? Integer.parseInt(num) : 0000000);
    }
}
