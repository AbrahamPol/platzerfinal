/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jose;

import conexion.Conexion;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletConfig;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

/**
 *
 * @author Hernandez
 */
@WebServlet(name = "ServRegDetallesTour", urlPatterns = {"/ServRegDetallesTour"})
@MultipartConfig
public class ServRegDetallesTour extends HttpServlet {

    ResultSet cdr = null;
    Statement sentenciaSQL = null;
    Conexion conecta = new Conexion();
    
    public void init(ServletConfig config) throws ServletException{
       super.init(config);
       conecta.Conectar();
       sentenciaSQL = conecta.getSentenciaSQL();
    }
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServRegDetallesTour</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ServRegDetallesTour at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html:charset=UTF-8");
        String empresa = request.getParameter("empresa");
        try {
            cdr = sentenciaSQL.executeQuery("SELECT * FROM TOUR WHERE NOMBREEMPRESA = '"+empresa+"' ");
            if(cdr.next()){
            out.println("<center>"
                    + "<form id=formDetallesPDF enctype=multipart/form-data>"
                    + "<h3>Subir Pdf Detalles de Tour</h3><br/>"
                    + "<select name=nomDetTour id=nomDetTour  class=form-control>"
                    + "<option>Selecciona el Nombre</option>");
            
                cdr = sentenciaSQL.executeQuery("SELECT NOMBRE FROM TOUR WHERE NOMBREEMPRESA = '"+empresa+"' ");    
                while (cdr.next()){
                out.println("<option value='"+cdr.getString(1)+"'>"+cdr.getString(1)+"</option>");                
            }
                Consultas c = new Consultas();
                c.consultarEmpresa(empresa);                
                out.println("</select><br/><br/>"
                        + "<input type=hidden id=empresaDet name=empresa value='"+empresa+"'><br/><br/>"                                                
                        + "<input type=\"file\" class=\"form-control-lg form-control-file\" accept=\"application/pdf\" name=\"pdf\" id=pdf><br/><br/>"
                        + "<input type=\"button\" value=Guardar onclick=subirPDF(); class=\"btn btn-theme btn-default\">"
                        + "</form>"
                        + "<div id=subirDetalles></div>");
                out.println("</center>");
            }
            else{
                out.println("<center><h3>Actualmente no cuentas con tours registrados</h3></center>");
            }
                
        } catch (SQLException ex) {
            ex.getMessage();
            ex.printStackTrace();
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html:charset=UTF-8");
        DetallesTour dt = new DetallesTour();
        String nombre = request.getParameter("nomDetTour").toUpperCase();
        String empresa = request.getParameter("empresa");
        Part filePart = request.getPart("pdf");
//        String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
        InputStream fileContent = filePart.getInputStream(); 
        Foto f = new Foto();
        int contador = 0;
        try {
            HttpSession session = request.getSession();
            session.setMaxInactiveInterval(70*60);
            boolean final_ar = false;
        int datos_entrada[] = new int[(int)filePart.getSize()];
        contador = 0;
        final_ar = false;
            
            while(!final_ar){
                int byte_entrada = fileContent.read();
                if(byte_entrada != -1)
                    datos_entrada[contador] = byte_entrada;
                else
                    final_ar = true;
//                System.out.println(contador + " " + datos_entrada[contador]);
                contador++;
            }
            dt.inputStream(datos_entrada, nombre, empresa);
            dt.guardarRuta(nombre,empresa);
//            f.mostrarimagen();
//            out.println("<img src="+f.getNombre()+">");
        } catch(IOException e){
            System.out.println("Causa"+e.getCause());
            e.printStackTrace();
        }        
        System.out.println(contador);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
