/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jose;

import conexion.Conexion;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Cruz
 */
@WebServlet(name = "ServConsTours", urlPatterns = {"/ServConsTours"})
public class ServConsTours extends HttpServlet {

    ResultSet cdr = null;
    Statement sentenciaSQL = null;
    Conexion conecta = new Conexion();
    
    public void init(ServletConfig config) throws ServletException{
       super.init(config);
       conecta.Conectar();
       sentenciaSQL = conecta.getSentenciaSQL();
    }
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServConsTours</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ServConsTours at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html:charset=UTF-8");
        PrintWriter out = response.getWriter();
        String empresa = request.getParameter("empresa");
        try{            
            cdr = sentenciaSQL.executeQuery("SELECT * FROM TOUR WHERE NOMBREEMPRESA = '"+empresa+"' ");
            
//            Mostrar resultados obtenidos
            out.println("<html><head><title>Registro de Tutores</title></head>");
            out.println("<body><center><h3>Consulta de Tours Registrados</h3></center><br/>");
//            Tabla
            out.println("<table class=\"table table-striped\">");
//            Cabeceras
            out.println("<tr>");
            
            out.println("<th>#</th>" + "<th>NOMBRE</th>" + "<th>COSTO</th>" + "<th>DESCRIPCIÓN</th>" + "<th>ESTADO</th>" + "<th>DETALLES</th>" + "<th>IMAGEN</th>");
            out.println("</tr>");
            int c = 1;
            while(cdr.next()){
                out.println("<tr>");                
                out.println("<td width=10%>" + c++ + "</td>");
                out.println("<td width=30%>" + cdr.getString(3) + "</td>");
                out.println("<td width=10%>$" + cdr.getString(4) + "</td>");
                out.println("<td width=30%>" + cdr.getString(5) + "</td>");                
                out.println("<td width=10%>" + cdr.getString(6) + "</td>");                
                out.println("<td width=10%>" + cdr.getString(8) + "</td>");                
                out.println("<td width=10%>" + cdr.getString(10) + "</td>");                
                out.println("</tr>");
            }
//            Cierre de tabla
            out.println("</table>");
            
//            Hipervinculo de regreso a la página Pilotos            
        } catch (SQLException ex) {
            out.print("Excepción SQL: " + ex.getMessage());
        } catch (NullPointerException e) {
            out.print("Apuntando SQL: " + e.getMessage());
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
               
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
