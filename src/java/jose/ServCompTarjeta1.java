/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jose;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Hernandez
 */
@WebServlet(name = "ServCompTarjeta1", urlPatterns = {"/ServCompTarjeta1"})
@MultipartConfig
public class ServCompTarjeta1 extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServCompTarjeta1</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ServCompTarjeta1 at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        Tarjeta t = new Tarjeta();
        try (PrintWriter out = response.getWriter()) {
            String nombreT = request.getParameter("nombreT");
            double costo = Double.parseDouble(request.getParameter("costo"));
            String empresa = request.getParameter("empresa");
            String notarjeta = request.getParameter("notarjeta");
        t.consultarTarjeta(notarjeta);
        if(t.getSaldo() > costo) {
            out.println("<strong>Pago realizado con éxito</strong><br>");
            t.actualizarSaldo(notarjeta, costo, t.getSaldo());
            out.println("<iframe src="+Contante.RUTA_COMPROBANTE_C+empresa.replaceAll(" ", "")+"_"+nombreT.replaceAll(" ", "")+".pdf"+" scrolling=\"no\" align=\"middle\" width=\"100%\" height=\"890\" frameborder=\"no\" allowtransparency=\"true\" background-color=\"transparent\">Transacción exitosa</strong><br/>");
        }
        else if (t.getSaldo() < costo)
            out.println("no");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
