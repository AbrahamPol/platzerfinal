/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jose;

import conexion.Conexion;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

/**
 *
 * @author Hernandez
 */
@WebServlet(name = "ServRespCotizacion", urlPatterns = {"/ServRespCotizacion"})
@MultipartConfig
public class ServRespCotizacion extends HttpServlet {

    ResultSet cdr = null;
    Statement sentenciaSQL = null;
    Conexion conecta = new Conexion();
    
    public void init(javax.servlet.ServletConfig config) throws ServletException{
       super.init(config);
       conecta.Conectar();
       sentenciaSQL = conecta.getSentenciaSQL();
    }
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServRespCotizacion</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ServRespCotizacion at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        int idcotizacion = Integer.parseInt(request.getParameter("idcotizacion"));
        String nombreu = request.getParameter("nombreu");
        String apellido = request.getParameter("apellido");
        String destino = request.getParameter("destino");
        String empresa = request.getParameter("empresa");
        Part archivo = request.getPart("pdf");
        String fileName = Paths.get(archivo.getSubmittedFileName()).getFileName().toString();        
        InputStream fileContent = archivo.getInputStream();
        System.out.println(fileName);
        Correo c = new Correo();
        c.actualizarEstado(idcotizacion);
        int contador = 0;
        try {
            HttpSession session = request.getSession();
            session.setMaxInactiveInterval(70*60);
            boolean final_ar = false;
            int datos_entrada[] = new int[(int)archivo.getSize()];
            contador = 0;
            final_ar = false;
            
            while(!final_ar){
                int byte_entrada = fileContent.read();
                if(byte_entrada != -1)
                    datos_entrada[contador] = byte_entrada;
                else
                    final_ar = true;
//                System.out.println(contador + " " + datos_entrada[contador]);
                contador++;
            }
            c.outputStream(datos_entrada, fileName);
            c.correoCotizacion(nombreu, apellido, destino, fileName, empresa);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
