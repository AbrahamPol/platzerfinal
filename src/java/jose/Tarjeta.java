/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jose;
import conexion.ConexionBanco;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Hernandez
 */
public class Tarjeta {
    
    ResultSet cdr = null;
    Statement sentenciaSQL = null;
    ConexionBanco conecta = new ConexionBanco();
    
    private String idtarjeta;
    private String notarjeta;
    private String titular;
    private String mesex;
    private String anoex;
    private String clave;
    private double saldo;
    
    public void consultarTarjeta(String notar){
        conecta.ConectarB();
        sentenciaSQL = conecta.getSentenciaSQLB();
        try {
        cdr = sentenciaSQL.executeQuery("SELECT * FROM TARJETA WHERE NOTARJETA = "+notar+" ");
        while(cdr.next()){
            idtarjeta = String.valueOf(cdr.getInt(1));
            notarjeta = cdr.getString(2);
            titular = cdr.getString(3);
            mesex = cdr.getString(4);
            anoex = cdr.getString(5);
            clave = cdr.getString(6);
            saldo = cdr.getDouble(7);
        }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void actualizarSaldo(String id,double costo,double saldo){
        conecta.ConectarB();
        sentenciaSQL = conecta.getSentenciaSQLB();
        System.out.println(id);
        System.out.println(costo);
        System.out.println(saldo);
        double nuevoSaldo = (saldo - costo);
        try {
            sentenciaSQL.executeUpdate("UPDATE TARJETA SET SALDO = "+nuevoSaldo+" WHERE NOTARJETA = '"+id+"' ");
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * @return the idtarjeta
     */
    public String getIdtarjeta() {
        return idtarjeta;
    }

    /**
     * @param idtarjeta the idtarjeta to set
     */
    public void setIdtarjeta(String idtarjeta) {
        this.idtarjeta = idtarjeta;
    }

    /**
     * @return the notarjeta
     */
    public String getNotarjeta() {
        return notarjeta;
    }

    /**
     * @param notarjeta the notarjeta to set
     */
    public void setNotarjeta(String notarjeta) {
        this.notarjeta = notarjeta;
    }

    /**
     * @return the titular
     */
    public String getTitular() {
        return titular;
    }

    /**
     * @param titular the titular to set
     */
    public void setTitular(String titular) {
        this.titular = titular;
    }

    /**
     * @return the mesex
     */
    public String getMesex() {
        return mesex;
    }

    /**
     * @param mesex the mesex to set
     */
    public void setMesex(String mesex) {
        this.mesex = mesex;
    }

    /**
     * @return the anoex
     */
    public String getAnoex() {
        return anoex;
    }

    /**
     * @param anoex the anoex to set
     */
    public void setAnoex(String anoex) {
        this.anoex = anoex;
    }

    /**
     * @return the clave
     */
    public String getClave() {
        return clave;
    }

    /**
     * @param clave the clave to set
     */
    public void setClave(String clave) {
        this.clave = clave;
    }

    /**
     * @return the saldo
     */
    public double getSaldo() {
        return saldo;
    }

    /**
     * @param saldo the saldo to set
     */
    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }
}
