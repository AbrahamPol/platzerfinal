package jose;

import conexion.Conexion;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import jose.Contante;


public class Correo {
    
    ResultSet cdr = null;
    Statement sentenciaSQL = null;
    Conexion conecta = new Conexion();
    
    public void correoCotizacion(String nombreu,String apellido,String destinot,String nombreArchivo,String empresa){
        try {
            Properties props = new Properties();
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.setProperty("mail.smtp.starttls.enable", "true");
            props.setProperty("mail.smtp.port", "587");
            props.setProperty("mail.smtp.user", "cotizaciones.platzer@gmail.com");
            props.setProperty("mail.smtp.auth", "true");

            Session session = Session.getDefaultInstance(props, null);
            // session.setDebug(true);

            // Se compone la parte del texto
            BodyPart texto = new MimeBodyPart();
            texto.setText("Hola " + nombreu + " " + apellido + " te mandamos respuesta de la cotizacion del tour con destino a " + destinot);

            // Se compone el adjunto con la imagen
            BodyPart adjunto = new MimeBodyPart();
            adjunto.setDataHandler(
                new DataHandler(new FileDataSource(Contante.RUTA_PDF_CORREO+nombreArchivo)));
            adjunto.setFileName(Contante.RUTA_PDF_CORREO+nombreArchivo);

            // Una MultiParte para agrupar texto e imagen.
            MimeMultipart multiParte = new MimeMultipart();
            multiParte.addBodyPart(texto);
            multiParte.addBodyPart(adjunto);

            // Se compone el correo, dando to, from, subject y el
            // contenido.
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress("cotizaciones.platzer@gmail.com"));
            message.addRecipient(
                Message.RecipientType.TO,
                new InternetAddress("2517160183jhernandezc@gmail.com"));
            message.setSubject(empresa + " Cotización de Tour");
            message.setContent(multiParte);

            // Se envia el correo.
            Transport t = session.getTransport("smtp");
            t.connect("cotizaciones.platzer@gmail.com", "qbssxkyhhgnxbxob");
            t.sendMessage(message, message.getAllRecipients());
            t.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void outputStream(int datos_entrada[],String nombre){
        try {
            FileOutputStream imagen = new FileOutputStream(Contante.RUTA_PDF_CORREO+nombre);
            for(int i=0;i<datos_entrada.length;i++){
                imagen.write(datos_entrada[i]);
//                System.out.println(datos_entrada[i]);
            }            
            imagen.close();            
        } catch (IOException ex) {
            ex.getMessage();
            ex.printStackTrace();
        }
    }
    
    public void actualizarEstado(int idCotizacion){
        conecta.Conectar();
        sentenciaSQL = conecta.getSentenciaSQL();
        try {
            sentenciaSQL.executeUpdate("UPDATE COTIZACIONTOUR SET ESTADO = 'CONTESTADO' WHERE IDCOTIZACION = "+idCotizacion+" ");
        } catch (SQLException ex) {
            Logger.getLogger(Correo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
//    public static void main(String[] args) {
//        Correo c = new Correo();
//        c.correoCotizacion();
//    }
}