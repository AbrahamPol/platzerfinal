/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jose;

import conexion.Conexion;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Hernandez
 */
public class Consultas {
    
    ResultSet cdr = null;
    Statement sentenciaSQL = null;
    Conexion conecta = new Conexion();
    
    private int id;
    private String nombreEmpresa;
    private String nombreTour;
    private double costo;
    private String descripcion;
    private String estado;
    private String ruta;    
    private String nombrePdf;
    
    private int idLinea;
    private int idusuarioLinea;
    private String nombreTourLinea;
    private String fechaLineaC;
    private String rutalinea;
    private String nombreLinea;
    
    public void consultarEmpresa(String empresa){
        conecta.Conectar();
        sentenciaSQL = conecta.getSentenciaSQL();
        try {
            cdr = sentenciaSQL.executeQuery("SELECT * FROM TOUR WHERE NOMBREEMPRESA = '"+empresa+"' ");
            while(cdr.next()){
                id = cdr.getInt(1);
                nombreEmpresa = cdr.getString(2);
                nombreTour = cdr.getString(3);
                costo = cdr.getDouble(4);
                descripcion = cdr.getString(5);
            }
        } catch (SQLException ex) {
            ex.getMessage();
            ex.printStackTrace();
        }
    }
    
    public void consultarTour(String nombre){
        conecta.Conectar();
        sentenciaSQL = conecta.getSentenciaSQL();
        try {
            cdr = sentenciaSQL.executeQuery("SELECT * FROM TOUR WHERE NOMBRE = '"+nombre+"' ");
            while(cdr.next()){
                id = cdr.getInt(1);
                nombreEmpresa = cdr.getString(2).toLowerCase();
                nombreTour = cdr.getString(3);
                costo = cdr.getDouble(4);
                descripcion = cdr.getString(5);
            }
        } catch (SQLException ex) {
            ex.getMessage();
            ex.printStackTrace();
        }
    }
    
    public void consultarLC(int idusuario,String nombreT,String fecha){
        conecta.Conectar();
        sentenciaSQL = conecta.getSentenciaSQL();
        try {
            cdr = sentenciaSQL.executeQuery("SELECT * FROM LINEACAPTURA WHERE IDUSUARIO = "+idusuario+" AND NOMBRET = '"+nombreT+"' AND FECHA = '"+fecha+"' ");
            while(cdr.next()){
                idLinea = cdr.getInt(1);
                idusuarioLinea = cdr.getInt(2);
                nombreTourLinea = cdr.getString(3);
                fechaLineaC = cdr.getString(4);
                rutalinea = cdr.getString(5);
                nombreLinea = cdr.getString(6);                
            }
        } catch (SQLException ex) {
            ex.getMessage();
            ex.printStackTrace();
        }
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the nombreEmpresa
     */
    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    /**
     * @param nombreEmpresa the nombreEmpresa to set
     */
    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    /**
     * @return the nombreTour
     */
    public String getNombreTour() {
        return nombreTour;
    }

    /**
     * @param nombreTour the nombreTour to set
     */
    public void setNombreTour(String nombreTour) {
        this.nombreTour = nombreTour;
    }

    /**
     * @return the costo
     */
    public double getCosto() {
        return costo;
    }

    /**
     * @param costo the costo to set
     */
    public void setCosto(double costo) {
        this.costo = costo;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * @return the ruta
     */
    public String getRuta() {
        return ruta;
    }

    /**
     * @param ruta the ruta to set
     */
    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    /**
     * @return the nombrePdf
     */
    public String getNombrePdf() {
        return nombrePdf;
    }

    /**
     * @param nombrePdf the nombrePdf to set
     */
    public void setNombrePdf(String nombrePdf) {
        this.nombrePdf = nombrePdf;
    }

    /**
     * @return the idLinea
     */
    public int getIdLinea() {
        return idLinea;
    }

    /**
     * @param idLinea the idLinea to set
     */
    public void setIdLinea(int idLinea) {
        this.idLinea = idLinea;
    }

    /**
     * @return the rutalinea
     */
    public String getRutalinea() {
        return rutalinea;
    }

    /**
     * @param rutalinea the rutalinea to set
     */
    public void setRutalinea(String rutalinea) {
        this.rutalinea = rutalinea;
    }

    /**
     * @return the nombreLinea
     */
    public String getNombreLinea() {
        return nombreLinea;
    }

    /**
     * @param nombreLinea the nombreLinea to set
     */
    public void setNombreLinea(String nombreLinea) {
        this.nombreLinea = nombreLinea;
    }

    /**
     * @return the idusuarioLinea
     */
    public int getIdusuarioLinea() {
        return idusuarioLinea;
    }

    /**
     * @param idusuarioLinea the idusuarioLinea to set
     */
    public void setIdusuarioLinea(int idusuarioLinea) {
        this.idusuarioLinea = idusuarioLinea;
    }

    /**
     * @return the nombreTourLinea
     */
    public String getNombreTourLinea() {
        return nombreTourLinea;
    }

    /**
     * @param nombreTourLinea the nombreTourLinea to set
     */
    public void setNombreTourLinea(String nombreTourLinea) {
        this.nombreTourLinea = nombreTourLinea;
    }

    /**
     * @return the fechaLineaC
     */
    public String getFechaLineaC() {
        return fechaLineaC;
    }

    /**
     * @param fechaLineaC the fechaLineaC to set
     */
    public void setFechaLineaC(String fechaLineaC) {
        this.fechaLineaC = fechaLineaC;
    }
}