/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jose;

import conexion.Conexion;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Hernandez
 */
public class DetallesTour {
    
    ResultSet cdr = null;
    Statement sentenciaSQL = null;
    Conexion conecta = new Conexion();
    
    private int id;
    private String nombreEmpresa;
    private String nombreTour;
    private double costo;
    private String descripcion;
    private String estado;
    private String ruta;    
    private String nombrePdf;
    
    Consultas c = new Consultas();
    
    public void inputStream(int datos_entrada[],String nombre,String empresa){
        try {
            nombre = nombre.replaceAll(" ", "");
            System.out.println(empresa+" "+nombre);
            FileOutputStream imagen = new FileOutputStream(Contante.RUTA_PDF+empresa.toLowerCase().replaceAll(" ", "")+"_"+nombre.toLowerCase()+".pdf");
            for(int i=0;i<datos_entrada.length;i++){
                imagen.write(datos_entrada[i]);
//                System.out.println(datos_entrada[i]);
            }            
            imagen.close();            
        } catch (IOException ex) {
            ex.getMessage();
            ex.printStackTrace();
        }
    }
    
    public void guardarRuta(String nombre,String empresa){
        conecta.Conectar();
        sentenciaSQL = conecta.getSentenciaSQL();
        try {
            String nombrepdf = nombre.replaceAll(" ", "");
            sentenciaSQL.executeUpdate("UPDATE TOUR SET RUTA = '"+Contante.RUTA_PDF+"', NOMBREPDF = '"+empresa.toLowerCase().replaceAll(" ", "")+"_"+nombrepdf.toLowerCase()+".pdf' WHERE NOMBRE = '"+nombre+"' ");
            System.out.println("Foto registrada");
        } catch (SQLException ex) {
            ex.getMessage();
            ex.printStackTrace();
        }
    }          
}