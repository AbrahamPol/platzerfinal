/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jose;

import conexion.ConexionBanco;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Hernandez
 */
@WebServlet(name = "ServCompTarjeta", urlPatterns = {"/ServCompTarjeta"})
@MultipartConfig
public class ServCompTarjeta extends HttpServlet {

    ResultSet cdr = null;
    Statement sentenciaSQL = null;
    ConexionBanco conecta = new ConexionBanco();
    
    public void init(javax.servlet.ServletConfig config) throws ServletException{
       super.init(config);
       conecta.ConectarB();
       sentenciaSQL = conecta.getSentenciaSQLB();
    }
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServCompTarjeta</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ServCompTarjeta at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()){
            String nombre = request.getParameter("nombre");
            double costo = Double.parseDouble(request.getParameter("costo"));
            String descripcion = request.getParameter("descripcion");
            int cantidad = Integer.parseInt(request.getParameter("cantidad"));
            String fecha = request.getParameter("fecha");
            int idusuario = Integer.parseInt(request.getParameter("idusuario"));
        
        out.println("<div class=newsfeed><h4><strong>Resumen de compra del tour "+nombre+"</strong></h4>");
        out.println("Costo: " + costo + "<br/>");
        out.println("Descripción: " + descripcion + "<br/>");
        out.println("Cantidad de Personas: " + cantidad + "<br/>");
        out.println("Fecha de Salida: " + fecha + "<br/><br/>");
        out.println("<center><h4><strong>Completa el siguiente formulario:</strong></h4><br/>");
        out.println("<form id=formPTarj enctype=multipart/form-data>");
        out.println("<input type=text class=input-lg name=notarjeta id=notarjetaPtar maxlength=\"16\" placeholder=\"Numero de Tarjeta*\" autofocus onkeypress=\"return soloNumeros(event)\">");
        out.println("<input type=text class=input-lg name=titular id=titularPtar placeholder=Titular* onkeypress=\"return soloLetras(event)\">");
        out.println("<input type=text class=input-lg name=mes id=mesPtar maxlength=\"2\" placeholder=\"Mes de Expiración*\" onkeypress=\"return soloNumeros(event)\"><br/><br/>");
        out.println("<input type=text class=input-lg name=ano id=anoPtar maxlength=\"4\" placeholder=\"Año de Expiración*\" onkeypress=\"return soloNumeros(event)\">");
        out.println("<input type=text class=input-lg name=clave id=clavePtar maxlength=\"3\" placeholder=\"Clave de Seguridad*\" onkeypress=\"return soloNumeros(event)\"><br/><br/>");
        out.println("<input type=button class=\"btn btn-default btn-theme\" value=Siguiente onclick=pagarTourTarjeta1();><br/><br/>");
        out.println("<div id=rPagoTarjeta></div>");
        out.println("<input type=hidden name=idusuario value="+idusuario+"><br/>");
        out.println("<input type=hidden name=nombreT value='"+nombre+"'><br/>");
        out.println("<input type=hidden name=costo value='"+costo+"'><br/>");
        out.println("<input type=hidden name=descripcion value='"+descripcion+"'><br/>");
        out.println("<input type=hidden name=cantidad value='"+cantidad+"'><br/>");
        out.println("<input type=hidden name=fecha value='"+fecha+"'><br/>");
        out.println("</form><br/><br/>");
        out.println("</center></div>");
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        Tarjeta t = new Tarjeta();
        ComprobanteDC com = new ComprobanteDC();
        Consultas c = new Consultas();
        try (PrintWriter out = response.getWriter()) {
            String notarjeta = request.getParameter("notarjeta");
            String titular = request.getParameter("titular");
            String mes = request.getParameter("mes");
            String ano = request.getParameter("ano");
            String clave = request.getParameter("clave");
            int idusuario = Integer.parseInt(request.getParameter("idusuario"));
            String nombreT = request.getParameter("nombreT");
            double costo = Double.parseDouble(request.getParameter("costo"));
            String descripcion = request.getParameter("descripcion");
            int cantidad = Integer.parseInt(request.getParameter("cantidad"));
            String fecha = request.getParameter("fecha");
            
            t.consultarTarjeta(notarjeta);
            c.consultarTour(nombreT);
            if(t.getNotarjeta().equals(notarjeta) && t.getTitular().equals(titular) && t.getMesex().equals(mes) && t.getAnoex().equals(ano) && t.getClave().equals(clave)){
                out.println("<div class=newsfeed><h4><strong>Resumen de compra del tour "+nombreT+"</strong></h4>");
                out.println("Costo: " + costo + "<br/>");
                out.println("Descripción: " + descripcion + "<br/>");
                out.println("Cantidad de Personas: " + cantidad + "<br/>");
                out.println("Fecha de Salida: " + fecha + "<br/><br/>");
//                out.println("<center><input type=button class=\"btn btn-default btn-theme\" value=Regresar onclick=pagarTourTarjeta1();>");
                out.println("<input type=button class=\"btn btn-default btn-theme\" value=Pagar onclick=pagarTourTarjeta2();><br/><br/>");
                out.println("<div id=respuestaTarjeta><div>");
                out.println("<form id=formPTarj1 enctype=multipart/form-data>");
                out.println("<input type=hidden name=idusuario value="+idusuario+">");
                out.println("<input type=hidden name=nombreT value='"+nombreT+"'>");
                out.println("<input type=hidden name=costo value='"+costo+"'>");
                out.println("<input type=hidden name=descripcion value='"+descripcion+"'>");
                out.println("<input type=hidden name=cantidad value='"+cantidad+"'>");
                out.println("<input type=hidden name=fecha value='"+fecha+"'>");
                out.println("<input type=hidden name=empresa value='"+c.getNombreEmpresa()+"'>");
                out.println("<input type=hidden name=notarjeta value='"+notarjeta+"'>");
//                out.println("<input type=hidden name=notarjeta value='"+titular+"'>");
//                out.println("<input type=hidden name=notarjeta value='"+mes+"'>");
//                out.println("<input type=hidden name=notarjeta value='"+ano+"'>");
//                out.println("<input type=hidden name=notarjeta value='"+clave+"'>");
                out.println("</center></form></div>");
                com.generarPDF("Comprobante de Compra Platzer", "No es valido como factura. Para solicitar tu factura marca al 01 800 PLATZER", "¡Gracias por tu compra!", Contante.RUTA_LOGO+"logo.jpeg", Contante.RUTA_COMPROBANTE+c.getNombreEmpresa().replaceAll(" ", "")+"_"+nombreT.replaceAll(" ", "")+".pdf", "1234567",c.getNombreEmpresa(),nombreT,costo,descripcion,fecha,cantidad);
            } else 
                out.println("no");
//                out.println("<strong>Puede que algún dato del formulario sea incorrecto</strong>");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
