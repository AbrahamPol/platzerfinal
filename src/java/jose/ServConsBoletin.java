/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jose;

import conexion.ConexionBanco;
import conexion.Conexion;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Cruz
 */
@WebServlet(name = "ServConsBoletin", urlPatterns = {"/ServConsBoletin"})
public class ServConsBoletin extends HttpServlet {

    ResultSet cdr = null;
    Statement sentenciaSQL = null;
    Conexion conecta = new Conexion();
    
    ResultSet cdrB = null;
    Statement sentenciaSQLB = null;
    ConexionBanco conectaB = new ConexionBanco();
    
    public void init(ServletConfig config) throws ServletException{
       super.init(config);
       conecta.Conectar();
       sentenciaSQL = conecta.getSentenciaSQL();
       conectaB.ConectarB();
       sentenciaSQLB = conectaB.getSentenciaSQLB();
    }
        
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServConsBoletin</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ServConsBoletin at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html:charset=UTF-8");
        PrintWriter out = response.getWriter(); 
        int idusuario = Integer.parseInt(request.getParameter("idusuario"));
        try {
            cdr = sentenciaSQL.executeQuery("SELECT ESTADO FROM SBOLETIN WHERE IDUSUARIO = "+idusuario+" ");
            if(cdr.next()){
                String estado = cdr.getString(1);
                if(estado.equalsIgnoreCase("SUSCRITO")){
                    out.println("<br/><br/><div class=\"col-md-12\"><iframe src=pdf/Linea.pdf scrolling=\"no\" align=\"middle\" width=\"100%\" height=\"890\" frameborder=\"no\" allowtransparency=\"true\" background-color=\"transparent\"></iframe><br/><br/>");
                    out.println("<input type=button  class=\"btn btn-theme btn-block\" value=\"Cancelar Suscripción\" onclick=cancelarBoletin();></div><br/>"); 
                }
                else{
                    out.println("<br/><div class=\"col-md-12\"><input type=button  class=\"btn btn-theme btn-block\" value=\"Suscribirme\" onclick=suscribirBoletin();></div><br/>"); 
                }
            }
        } catch (SQLException ex) {
            ex.getMessage();
            ex.printStackTrace();
        }                                               
    
    }                     

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html:charset=UTF-8");
        PrintWriter out = response.getWriter();
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
