/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jose;

import conexion.Conexion;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Cruz
 */
@WebServlet(name = "ServMostTours", urlPatterns = {"/ServMostTours"})
public class ServMostTours extends HttpServlet {

    ResultSet cdr = null;
    Statement sentenciaSQL = null;
    Conexion conecta = new Conexion();
    
    public void init(ServletConfig config) throws ServletException{
       super.init(config);
       conecta.Conectar();
       sentenciaSQL = conecta.getSentenciaSQL();
    }
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServMostTours</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ServMostTours at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html:charset=UTF-8");
        PrintWriter out = response.getWriter();         
        out.println("<section class=\"newsfeed\">\n" +
"	  <div class=\"container\">" +
                "<input type=button class=\"btn btn-default col-xs-12 btn-theme\" value=\"Cotiza Tours\" onclick=\"DinamicoDiv('cotizar');\">"+
"	  " +
"	   ");
        out.println("<br/><br/><div class=\"row top-row\">");
        
        int c = 1;
        
        try {            
            cdr = sentenciaSQL.executeQuery("SELECT * FROM TOUR WHERE ESTADO = 'ALTA' ");            
            while(cdr.next()){
                
                out.println("<div class=\"col-lg-3\">\n" +
"		 <div class=\"tr-section\">\n" +
"		  <div class=\"tr-post\">\n" +
"		   <div class=\"entry-header\">\n" +
"		    <div class=\"entry-thumbnail\">\n" +
"		     <a href=\"#\"><img class=\"img-fluid\" src=\"assets/img/tours/"+cdr.getString(10)+"\" alt=\"Tour\"></a>\n" +
"		    </div><!-- /entry-thumbnail -->\n" +
"	       </div><!-- /entry-header -->\n" +
"		   <div class=\"post-content\">\n" +
"		    <div class=\"author-post text-center\">\n" +
"		     <a href=\"#\"><img class=\"img-fluid rounded-circle\" src=\"assets/img/users/"+c+++".jpg\" alt=\"Emresa\"></a>\n" + /*Como puedes ver
        esta escrita la ruta de la imagen y solo le estoy concatenando el nombre y la extensión. Lo que vas a guardar en la base es el nombre de la 
        imagen que sacas del <input type=file> y tu guardas manualmente la imagen en la ruta.*/
"		    </div><!-- /author -->\n" +
"			<div class=\"card-content\">\n" +
"			 <h4>"+cdr.getString(3)+"</h4>\n" +
"		     <span>$"+cdr.getString(4)+"</span><br/>\n" +
    "		     <span>"+cdr.getString(5)+"</span>\n" +
"			</div>\n" +
"			 <a href='pdf/"+cdr.getString(8)+"' target=\"_blank\" class=\"kafe-btn kafe-btn-mint-small full-width\"> Detalles</a><br/>\n" +         
"			 <a href=\"#?empresaCP="+cdr.getString(3)+"\" onclick=\"DinamicoDiv('compraTour');mostrarFormaPago('tarjeta');\" class=\"kafe-btn kafe-btn-mint-small full-width\"> Comprar\n" +
"			 </a>		  \n" +
"		   </div><!-- /.post-content -->									\n" +
"		  </div><!-- /.tr-post -->	\n" +
"	     </div><!-- /.tr-post -->	\n" +
"		</div><!-- /col-sm-3 -->");
            }
        out.write("</div>"
                + "</div>"
                + "</section>");
        } catch (SQLException ex) {
            ex.getMessage();
            ex.printStackTrace();
        }             
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
