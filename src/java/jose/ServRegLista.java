/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jose;

import conexion.Conexion;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Hernandez
 */
@WebServlet(name = "ServRegLista", urlPatterns = {"/ServRegLista"})
public class ServRegLista extends HttpServlet {

    ResultSet cdr = null;
    Statement sentenciaSQL = null;
    Conexion conecta = new Conexion();
    
    public void init(ServletConfig config) throws ServletException{
       super.init(config);
       conecta.Conectar();
       sentenciaSQL = conecta.getSentenciaSQL();
    }
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServRegLista</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ServRegLista at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html:charset=UTF-8");
        out.println("<center>"
                + "<div class=login-wrap id=regform>"
                + "<h3>Agregar a la lista</h3>"
                + "<form>"
                + "<input type=text onkeypress=\"return soloLetras(event);\" class=form-control autofocus id=nombreLista placeholder=\"Nombre del Lugar*\" pattern=[^0-9] title=\"Sin números perro\"><br/><br/>"                
                + "<input type=button  class=\"btn btn-theme btn-defaul\" value=\"Agregar\" onclick=agregarLista();><br/><br/>"                                                        
                + ""
                + "</div>"                
                + "</center>");    
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html:charset=UTF-8");
        String nombre = request.getParameter("nombre").toUpperCase();
        
        try {
            sentenciaSQL.executeUpdate("INSERT INTO LISTA VALUES (NULL,1,'"+nombre+"') ");
            out.println("<div class=\"container-fluid\">\n" +
"                                        <div class=\"row\">                                            \n" +
"                                            <div class=\"col-md-4\">\n" +
"                                                <div class=\"alert alert-success\" id=\"gestionarLista\" >\n" +
"                                                    <button class=\"close\" data-dismiss=\"alert\"><span>&times</span></button>\n" +
                                                        "Se ha registrado Correctamente "+nombre+
"                                                </div>\n" +
"                                            </div>\n" +
"                                        </div>\n" +
"                                    </div>");
            System.out.println("Se ha agrada a tu lista "+nombre+" ");
        } catch (SQLException ex) {
            ex.getMessage();
            ex.printStackTrace();
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
