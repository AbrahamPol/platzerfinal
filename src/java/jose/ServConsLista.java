/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jose;

import conexion.Conexion;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Hernandez
 */
@WebServlet(name = "ServConsLista", urlPatterns = {"/ServConsLista"})
public class ServConsLista extends HttpServlet {

    ResultSet cdr = null;
    Statement sentenciaSQL = null;
    Conexion conecta = new Conexion();
    
    public void init(ServletConfig config) throws ServletException{
       super.init(config);
       conecta.Conectar();
       sentenciaSQL = conecta.getSentenciaSQL();
    }
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServConsLista</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ServConsLista at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html:charset=UTF-8");
        PrintWriter out = response.getWriter();        
        try{            
            cdr = sentenciaSQL.executeQuery("SELECT * FROM LISTA");
            
//            Mostrar resultados obtenidos            
            out.println("<body><center><h3>Mi Lista</h3></center><br/>");
//            Tabla
            out.println("<table class=\"table table-striped\">");
//            Cabeceras
            out.println("<tr>");
            
            out.println("<thead class=><th scope=\"col\">#</th><th scope=\"col\">NOMBRE DEL SITIO</th>" + "<th></th></thead>");
            out.println("</tr>");
            int c = 1;
            while(cdr.next()){
                out.println("<tr>");
                out.println("<td scope=\"row\">"+c+++"</td>"
                        + "<td>" + cdr.getString(3) + "</td>");
                out.println("<td><input type=button value=Eliminar class=\"btn btn-theme btn-default\" onclick=\"eliminarLista('"+cdr.getString(3)+"');\">");  
                
                out.println("</tr>");
            }
//            Cierre de tabla
            out.println("</table>");
            
            
//            Hipervinculo de regreso a la página Pilotos            
        } catch (SQLException ex) {
            out.print("Excepción SQL: " + ex.getMessage());
        } catch (NullPointerException e) {
            out.print("Apuntando SQL: " + e.getMessage());
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html:charset=UTF-8");
        PrintWriter out = response.getWriter();
        String id = request.getParameter("idlista");
        System.out.println("Se va a eliminar " + id);
        try {            
            sentenciaSQL.executeUpdate("DELETE FROM LISTA WHERE LUGAR = '"+id+"' ");
            out.println("");
        } catch (SQLException ex) {
            ex.getMessage();
            ex.printStackTrace();
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
