/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jose;

import conexion.Conexion;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Hernandez
 */
@WebServlet(name = "ServCotizarTour", urlPatterns = {"/ServCotizarTour"})
public class ServCotizarTour extends HttpServlet {

    ResultSet cdr = null;
    Statement sentenciaSQL = null;
    Conexion conecta = new Conexion();
    
    public void init(javax.servlet.ServletConfig config) throws ServletException{
       super.init(config);
       conecta.Conectar();
       sentenciaSQL = conecta.getSentenciaSQL();
    }
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServCotizarTour</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ServCotizarTour at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String nombre = request.getParameter("nombre").toUpperCase();
        String apellido = request.getParameter("apellido").toUpperCase();
        String correou = request.getParameter("correou");
        String destino = request.getParameter("destino").toUpperCase();
        String fecha = request.getParameter("fecha");
        int cantidad = Integer.parseInt(request.getParameter("cantidad"));
        String empresa = request.getParameter("empresa").toUpperCase();
        String correoe = request.getParameter("correoe");
        String comentario = request.getParameter("comentario").toUpperCase();
        try {
            cdr = sentenciaSQL.executeQuery("SELECT * FROM COTIZACIONTOUR WHERE NOMBREUSUARIO = '"+nombre+"' AND APELLIDO = '"+apellido+"' AND DESTINO = '"+destino+"' AND EMPRESA = '"+empresa+"' ");
            if(cdr.next()){
                out.println("Este formulario ya fue enviado. Si deseas otra cotización vuelve a llenar los campos");
            } else {
                sentenciaSQL.executeUpdate("INSERT INTO COTIZACIONTOUR VALUES (NULL,'"+nombre+"','"+apellido+"','"+correou+"','"+destino+"','"+fecha+"',"+cantidad+",'"+empresa+"','"+correoe+"','"+comentario+"',NOW(),NOW(),'RECIBIDO') ");
                out.println("Tu cotización fue enviada con exito, a la brevedad posible recibirás un correo de la empresa "+empresa+" con la cotización");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            ex.getMessage();
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
