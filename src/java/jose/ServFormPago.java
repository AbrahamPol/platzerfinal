/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jose;

import conexion.Conexion;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Hernandez
 */
@WebServlet(name = "ServFormPago", urlPatterns = {"/ServFormPago"})
public class ServFormPago extends HttpServlet {
    
    ResultSet cdr = null;
    Statement sentenciaSQL = null;
    Conexion conecta = new Conexion();
    
    public void init(javax.servlet.ServletConfig config) throws ServletException{
       super.init(config);
       conecta.Conectar();
       sentenciaSQL = conecta.getSentenciaSQL();
    }
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServFormPago</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ServFormPago at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String tipo = request.getParameter("tipo");
        String nombreTour = request.getParameter("tour");
        System.out.println(tipo);
        System.out.println(nombreTour);
        try (PrintWriter out = response.getWriter()) {
            cdr = sentenciaSQL.executeQuery("SELECT * FROM TOUR WHERE NOMBRE = '"+nombreTour+"' ");
            if(tipo.equalsIgnoreCase("tarjeta")){
                while(cdr.next()){
                    out.println("<div class=newsfeed><center>"
                        + "<h1>¡No te arrepentirás de tu compra!</h1></center><br/>"
                        + "<h4>Detalles de compra del Tour "+""+nombreTour+"" +": </h4>"
                        + "<h5>Compra con Tarjeta</h5>"
                        + "Nombre:<br/><input type=text class=form-control readonly=readonly id=nomTourCT1 value='"+cdr.getString(3)+"'><br/><br/>"
                        + "Costo:<br/><input type=text class=form-control readonly=readonly id=cosTourCT1 value='"+cdr.getString(4)+"'><br/><br/>"
                        + "Descripción:<br/><input type=text class=form-control readonly=readonly id=desTourCT1 value='"+cdr.getString(5)+"'><br/><br/>"
                        + "Cantidad de Personas:*<br/><input type=number class=form-control max=20 min=1 id=canTourCT1 value=1><br/><br/>"
                        + "Fecha:*<br/><input type=date class=form-control id=fechTourCT1><br/><br/>"
//                        + "<button class=\"btn btn-default btn-theme\" onclick=DinamicoDiv('tours');><span>Cancelar</span></button> "
                        + "<center><button class=\"btn btn-default btn-theme\" onclick=comprarTourTar1();><span>Siguente</span></button>"
                        + "</center></div>");
                }
            } else if(tipo.equalsIgnoreCase("oxxo")) {
                while(cdr.next()){
                    out.println("<div class=newsfeed><center>"
                        + "<h1>¡No te arrepentirás de tu compra!</h1></center><br/>"
                        + "<h4>Detalles de compra del Tour "+""+nombreTour+"" +": </h4>"
                        + "<h5>Compra en OXXO</h5>"
                        + "Nombre:<br/><input type=text class=form-control readonly=readonly id=nomTourCT2 value='"+cdr.getString(3)+"'><br/><br/>"
                        + "Costo:<br/><input type=text class=form-control readonly=readonly id=cosTourCT2 value='"+cdr.getString(4)+"'><br/><br/>"
                        + "Descripción:<br/><input type=text class=form-control readonly=readonly id=desTourCT2 value='"+cdr.getString(5)+"'><br/><br/>"
                        + "Cantidad de Personas*:<br/><input type=number class=form-control max=20 min=1 id=canTourCT2 value=1><br/><br/>"
                        + "Fecha*:<br/><input type=date class=form-control id=fechTourCT2><br/><br/>"
//                        + "<button class=\"btn btn-default btn-theme\" onclick=DinamicoDiv('tours');><span>Cancelar</span></button> "
                        + "<center><button class=\"btn btn-default btn-theme\" onclick=\"generarLC();\"><span>Generar Línea de Captura</span></button><br/><br/>"
                        + "<div id=pagoOxxo class=container-fluid>"
                        + "</center></div>");
                }
            }
        } catch (SQLException ex) {
            ex.getMessage();
            ex.printStackTrace();
        } 
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
