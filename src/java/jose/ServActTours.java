/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jose;

import conexion.Conexion;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Hernandez
 */
@WebServlet(name = "ServActTours", urlPatterns = {"/ServActTours"})
public class ServActTours extends HttpServlet {

    ResultSet cdr = null;
    Statement sentenciaSQL = null;
    Conexion conecta = new Conexion();
    
    public void init(ServletConfig config) throws ServletException{
       super.init(config);
       conecta.Conectar();
       sentenciaSQL = conecta.getSentenciaSQL();
    }
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServActTours</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ServActTours at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html:charset=UTF-8");
        PrintWriter out = response.getWriter();
        String nombre = request.getParameter("nombre");
        System.out.println(nombre);
        try {
            cdr = sentenciaSQL.executeQuery("SELECT * FROM TOUR WHERE NOMBRE = '"+nombre+"' ");
            while(cdr.next()){
                out.println("<center>"
                + "<div class=login-wrap id=regform>"
                + "<h3>Actualizar Tours</h3>"
                + "<input type=text value='"+cdr.getString(3)+"' class=form-control id=nombreAT placeholder=Nombre* readonly=readonly pattern=[^0-9] title=\"Sin números perro\"><br/><br/>"
                + "<input type=text value="+cdr.getDouble(4)+" class=form-control id=costoAT placeholder=Costo*><br/><br/>"
                + "<input type=text value='"+cdr.getString(5)+"' class=form-control id=descripcionAT placeholder=Descripción*><br/><br/>"
                + "<select id=estTour class=form-control>"
                + "<option value="+cdr.getString(6)+">"+cdr.getString(6)+"</option>"
                + "<option value=ALTA>ALTA</option>"
                + "<option value=BAJA>BAJA</option>"
                + "</select><br/><br/>"
                + "<input type=button  class=\"btn btn-theme btn-default\" value=\"Editar\" onclick=actualizarTour();><br/><br/>"                                                        
                + "</div>"
                + "<div id=actTour></div>"                
                + "</center>");
            }
        } catch (SQLException ex) {
            ex.getMessage();
            ex.printStackTrace();
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html:charset=UTF-8");
        String nombre = request.getParameter("nombre").toUpperCase();
        double costo = Double.parseDouble(request.getParameter("costo"));
        String estado = request.getParameter("estado");
        String descripcion = request.getParameter("descripcion").toUpperCase();                                        
            try {                                    
                sentenciaSQL.executeUpdate("UPDATE TOUR SET NOMBRE='"+nombre+"',COSTO="+costo+",DESCRIPCION='"+descripcion+"',ESTADO='"+estado+"' WHERE NOMBRE='"+nombre+"'  ");
                out.print("<font color=black>Tour actualizado exitosamente</font>");
                System.out.println("Tour "+nombre+" actualizado");            
        } catch (SQLException e) {
            e.printStackTrace();
            e.getMessage();
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
