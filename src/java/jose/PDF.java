/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jose;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.Barcode128;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfWriter;
import com.barcodelib.barcode.QRCode;
import java.io.FileOutputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
/**
 *
 * @author Hernandez
 */
public class PDF {
    
    int udm=0,resol=72,rot=0;
    float mi=0.000f, md=0.000f, ms=0.000f,min=0.000f,tam=5.00f;
    
    private Font fuenteBold = new Font(Font.FontFamily.COURIER, 24, Font.BOLD);    
    private Font fuenteNormal = new Font(Font.FontFamily.COURIER, 10, Font.NORMAL);    
    private Font fuenteItalic = new Font(Font.FontFamily.COURIER, 20, Font.ITALIC);

    public void generarPDF(String header,String info,String footer, String rutaImagen, String salida,String codigo){
        try {
            Document document = new Document(PageSize.A7,36,36,10,10);
            PdfWriter pw = PdfWriter.getInstance(document, new FileOutputStream(salida));
            document.open();
            document.add(getHeader(header));
            Image imagen = Image.getInstance(rutaImagen);
            imagen.scaleAbsolute(100, 75);
            imagen.setAlignment(Element.ALIGN_CENTER);
            document.add(imagen);
            document.add(getInfo(info));
            document.add(getInfo(""));
            document.add(getInfo(""));
            document.add(getInfo(""));
            document.add(getBarcode(document, pw, codigo));
            document.add(getInfo(""));
            document.add(getFooter(footer));
            document.close();
        } catch (Exception e) {
            e.getMessage();
            e.printStackTrace();
        }
    }
    
    public Paragraph getHeader(String texto){
        Paragraph p = new Paragraph();
        Chunk c = new Chunk();
        p.setAlignment(Element.ALIGN_CENTER);
        c.append(texto);
        c.setFont(fuenteBold);
        p.add(c);
        return p;
    }
    
    public Paragraph getInfo(String texto){
        Paragraph p = new Paragraph();
        Chunk c = new Chunk();
        p.setAlignment(Element.ALIGN_JUSTIFIED_ALL);
        c.append(texto);
        c.setFont(fuenteNormal);
        p.add(c);
        return p;
    }
    
    public Paragraph getFooter(String texto){
        Paragraph p = new Paragraph();
        Chunk c = new Chunk();
        p.setAlignment(Element.ALIGN_RIGHT);
        c.append(texto);
        c.setFont(fuenteItalic);
        p.add(c);
        return p;
    }
    
    private Image getBarcode(Document document, PdfWriter pw,String codigo){
        PdfContentByte cimg = pw.getDirectContent();
        Barcode128 code128 = new Barcode128();
        code128.setCode(formatearCodigo(codigo));
        code128.setCodeType(Barcode128.CODE128);
        code128.setTextAlignment(Element.ALIGN_CENTER);
        
        Image image = code128.createImageWithBarcode(cimg, BaseColor.BLACK, BaseColor.BLACK);
        float scaler = ((document.getPageSize().getWidth() - document.leftMargin() - document.rightMargin() - 0) / image.getWidth() * 60);
        image.scalePercent(scaler);
        image.setAlignment(Element.ALIGN_CENTER);
        return image;
    }
    
    private String formatearCodigo(String num){
        NumberFormat form = new DecimalFormat("0000000");
        return form.format((num != null) ? Integer.parseInt(num) : 0000000);
    }
    
    public void generarQR(String dato){
        try {
            QRCode c = new QRCode();
            c.setData(dato);
            c.setDataMode(QRCode.MODE_BYTE);
            c.setUOM(udm);
            c.setLeftMargin(mi);
            c.setRightMargin(md);
            c.setTopMargin(ms);
            c.setBottomMargin(min);
            c.setResolution(resol);
            c.setRotate(rot);
            c.setModuleSize(tam);
            String archivo = System.getProperty("C:\\Users\\Hernandez\\Desktop\\EncuestaDonacion.png");
            c.renderBarcode(archivo);            
        } catch (Exception e) {
            e.getMessage();
            e.printStackTrace();
        }
    }
    
    public static void main(String[] args) {
        PDF pdf = new PDF();
        pdf.generarPDF("Platzer", "Pago en Oxxo", "Gracias por su Compra", "C:\\Users\\Alumno\\Desktop\\Penguins.jpg", "C:\\Users\\Alumno\\Desktop\\prueba.pdf","1234567");
    }
}
