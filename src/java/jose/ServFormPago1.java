/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jose;

import conexion.Conexion;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Hernandez
 */
@WebServlet(name = "ServFormPago1", urlPatterns = {"/ServFormPago1"})
public class ServFormPago1 extends HttpServlet {

    ResultSet cdr = null;
    Statement sentenciaSQL = null;
    Conexion conecta = new Conexion();
    
    public void init(javax.servlet.ServletConfig config) throws ServletException{
       super.init(config);
       conecta.Conectar();
       sentenciaSQL = conecta.getSentenciaSQL();
    }
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServFormPago1</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ServFormPago1 at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String nombre = request.getParameter("nombre");
        double costo = Double.parseDouble(request.getParameter("costo"));
        String descripcion = request.getParameter("descripcion");
        String fecha = request.getParameter("fecha");
        int cantidad = Integer.parseInt(request.getParameter("cantidad"));
        try {
            cdr = sentenciaSQL.executeQuery("SELECT NOMBREEMPRESA FROM TOUR WHERE NOMBRE = '"+nombre+"' ");
            while(cdr.next()){
                String empresa = cdr.getString(1); 
                System.out.println(empresa);
                LineaCaptura lc = new LineaCaptura();
                String nombreT = ""+empresa.toLowerCase()+"_"+nombre.toLowerCase()+".pdf";
                lc.generarPDF("Línea de Captura Platzer", "Realiza tu pago en tu tienda OXXO más cercana", "¡Gracias por su compra!", Contante.RUTA_LOGO+"logo.jpeg", Contante.RUTA_LINEA_CAPTURA+nombreT.replaceAll(" ", ""), "1234567",empresa,nombre,costo,descripcion,fecha,cantidad);
                out.println("<div class=newsfeed><h4><strong>Resumen de compra del tour "+nombre+"</strong></h4>");
                out.println("Costo: " + costo + "<br/>");
                out.println("Descripción: " + descripcion + "<br/>");
                out.println("Cantidad de Personas: " + cantidad + "<br/>");
                out.println("Fecha de Salida: " + fecha + "<br/><br/>");
                out.println("<center><a href="+Contante.RUTA_LINEA_CAPTURA_C+nombreT.replaceAll(" ", "")+" target=\"_blank\" class=\"kafe-btn kafe-btn-mint-small\" width=100>Ver Linea de Captura</a> ");
                out.println("<a href="+Contante.RUTA_LINEA_CAPTURA_C+nombreT.replaceAll(" ", "")+" download class=\"kafe-btn kafe-btn-mint-small\" width=20%>Descargar Linea de Captura</a></center><br/>");
                out.println("</div>");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String nombre = request.getParameter("nombre");
        double costo = Double.parseDouble(request.getParameter("costo"));
        String descripcion = request.getParameter("descripcion");
        try {
            cdr = sentenciaSQL.executeQuery("SELECT NOMBREEMPRESA FROM TOUR WHERE NOMBRE = '"+nombre+"' ");
            if(cdr.next()){
//                String empresa = cdr.getString(1);                
//                LineaCaptura lc = new LineaCaptura();
//                String ruta = "E:\\JOSE\\Platzer\\web\\Fluffs\\pdf\\lineasC\\";
//                String nombreT = empresa+"_"+nombre+".pdf";  
                out.println("Jaja");                
//                lc.generarPDF("Línea de Captura Platzer", "Realiza tu pago en tu tienda OXXO más cercana", "¡Gracias por su compra!", "E:\\JOSE\\Platzer\\web\\Fluffs\\logo.jpeg", ruta+nombreT, "1234567",empresa,nombre,costo,descripcion);
//                out.println("<a href=#>Vemos</a>");
//                System.out.println("<a href=#>Vemos</a>");
//                lc.registrarLC(ruta, nombreT);
//                Consultas c = new Consultas();
//                c.consultarLC(nombreT);                
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
