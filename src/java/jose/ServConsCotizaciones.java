/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jose;

import conexion.Conexion;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Hernandez
 */
@WebServlet(name = "ServConsCotizaciones", urlPatterns = {"/ServConsCotizaciones"})
public class ServConsCotizaciones extends HttpServlet {

    ResultSet cdr = null;
    Statement sentenciaSQL = null;
    Conexion conecta = new Conexion();
    
    public void init(javax.servlet.ServletConfig config) throws ServletException{
       super.init(config);
       conecta.Conectar();
       sentenciaSQL = conecta.getSentenciaSQL();
    }
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServConsCotizaciones</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ServConsCotizaciones at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String empresa = request.getParameter("empresa");
        try {
            cdr = sentenciaSQL.executeQuery("SELECT * FROM COTIZACIONTOUR WHERE EMPRESA = '"+empresa+"' AND ESTADO = 'RECIBIDO' ORDER BY IDCOTIZACION DESC ");
            if(!cdr.next()){
                out.println("<ul><li>");
                out.println("<div class=\"media first_child\">");
                out.println("<div class=\"media_body\">");
                out.println("<center><h3>No hay cotizaciones nuevas</h3></center>");
                out.println("<div class=\"btn_group\">");                                                                                
                out.println("</div>");
                out.println("</div>");
                out.println("</div>");
                out.println("</li></ul>");
            } else {
                out.println("<ul>");
                do {
                    out.println("<li>");
                    out.println("<div class=\"media first_child\" id=respcoti>");
                    out.println("<img src=\"assets/img/users/1.jpg\" alt=\"\" class=\"img-responsive img-circle\">");
                    out.println("<div class=\"media_body\">");
                    out.println("<p><b>"+cdr.getString(2)+" "+cdr.getString(3)+"</b> quiere cotizar un tour</p>");
                    out.println("<p>Email: " + cdr.getString(4) + "<p/>");
                    out.println("<p>Destino: " + cdr.getString(5) + "<p/>");
                    out.println("<p>Fecha de Salida: " + cdr.getString(6) + "<p/>");
                    out.println("<p>Cantidad de Personas: " + cdr.getString(7) + "<p/>");
                    out.println("<p>Más detalles: " + cdr.getString(10) + "<p/>");
                    out.println("<form id=\"respC"+cdr.getString(1)+"\" name=\"responderCotizacion\" enctype=\"multipart/form-data\">");
                    out.println("<input type=hidden name=idcotizacion value='"+cdr.getString(1)+"'>");
                    out.println("<input type=hidden name=nombreu value='"+cdr.getString(2)+"'>");
                    out.println("<input type=hidden name=apellido value='"+cdr.getString(3)+"'>");
                    out.println("<input type=hidden name=destino value='"+cdr.getString(5)+"'>");
                    out.println("<input type=hidden name=empresa value='"+cdr.getString(8)+"'>");
                    out.println("<input type=file accept=\"application/pdf\" id=pdfCotizarCorreo"+cdr.getString(1)+" name=pdf>");
                    out.println("</form>");
                    out.println("<h6>"+cdr.getString(11)+" a las "+cdr.getString(12)+"</h6>");
                    out.println("<div class=\"btn_group\">");
                    out.println("<a class=\"kafe-btn kafe-btn-mint\" onclick=\"responderCoti('respC"+cdr.getString(1)+"','pdfCotizarCorreo"+cdr.getString(1)+"');consultarCotizaciones('"+cdr.getString(8)+"');\"><i class=\"fa fa-paper-plane\"></i></a>");
                    out.println("<a class=\"kafe-btn kafe-btn-red\" onclick=\"consultarCotizaciones('"+cdr.getString(8)+"');\"><i class=\"fa fa-times\"></i></a>");
                    out.println("</div>");
                    out.println("</div>");
                    out.println("</div>");
                    out.println("</li>");
                } while (cdr.next());
                out.println("</ul>");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
