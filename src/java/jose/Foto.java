package jose;

import conexion.Conexion;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hernandez
 */
public class Foto {   
    
    ResultSet cdr = null;
    Statement sentenciaSQL = null;
    Conexion conecta = new Conexion();
    
    public void inputStream(int datos_entrada[],String nombre,String empresa){
        try {
            nombre = nombre.replaceAll(" ", "");
            System.out.println(empresa+" "+nombre);
            FileOutputStream imagen = new FileOutputStream(Contante.RUTA_IMG+empresa.toLowerCase().replaceAll(" ", "")+"_"+nombre.toLowerCase()+".jpg");
            for(int i=0;i<datos_entrada.length;i++){
                imagen.write(datos_entrada[i]);
//                System.out.println(datos_entrada[i]);
            }            
            imagen.close();            
        } catch (IOException ex) {
            ex.getMessage();
            ex.printStackTrace();
        }
    }
    
    public void guardarRuta(String nombre,String empresa){
        conecta.Conectar();
        sentenciaSQL = conecta.getSentenciaSQL();
        try {
            String nombrepdf = nombre.replaceAll(" ", "");
            sentenciaSQL.executeUpdate("UPDATE TOUR SET RUTAIMG = '"+Contante.RUTA_IMG+"', NOMBREIMG = '"+empresa.toLowerCase().replaceAll(" ", "")+"_"+nombrepdf.toLowerCase()+".jpg' WHERE NOMBRE = '"+nombre+"' ");
            System.out.println("Foto registrada");
        } catch (SQLException ex) {
            ex.getMessage();
            ex.printStackTrace();
        }
    }
}
