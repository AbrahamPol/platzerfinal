/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jose;

import conexion.Conexion;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

/**
 *
 * @author Hernandez
 */
@WebServlet(name = "ServRegTours", urlPatterns = {"/ServRegTours"})
public class ServRegTours extends HttpServlet {

    ResultSet cdr = null;
    Statement sentenciaSQL = null;
    Conexion conecta = new Conexion();
    
    public void init(ServletConfig config) throws ServletException{
       super.init(config);
       conecta.Conectar();
       sentenciaSQL = conecta.getSentenciaSQL();
    }
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServRegTours</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ServRegTours at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html:charset=UTF-8");
        out.println("<center>"
                + "<div class=login-wrap id=regform>"
                + "<h3>Registrar Tours</h3>"
                + "<input autofocus type=text class=form-control id=nombre placeholder=Nombre* pattern=[^0-9] title=\"Sin números perro\"><br/><br/>"
                + "<input type=text class=form-control id=costo placeholder=Costo*><br/><br/>"
                + "<input type=text class=form-control id=descripcion placeholder=Descripción*><br/><br/>"                
                + "<input type=button  class=\"btn btn-theme btn-default\" value=\"Registrar\" onclick=registrarTour();><br/><br/>"                                                                   
                + "</div>"
                + "<div id=rT></div>"                
                + "</center>");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html:charset=UTF-8");
        String empresa = request.getParameter("empresa").toUpperCase();
        String nombre = request.getParameter("nombre").toUpperCase();
        double costo = Double.parseDouble(request.getParameter("costo"));
        String descripcion = request.getParameter("descripcion").toUpperCase();        
        try {
            cdr = sentenciaSQL.executeQuery("SELECT * FROM TOUR WHERE NOMBRE = '"+nombre+"' ");
            
            if (cdr.next()){
                out.println("El tour ya existe");
                System.out.println("El Tour "+nombre+" ya está registrado");
            }
            else{
                String empresa2 = empresa.replaceAll("\n", "");
                sentenciaSQL.executeUpdate("INSERT INTO TOUR VALUES (NULL,'"+empresa2+"','"+nombre+"',"+costo+",'"+descripcion+"','ALTA','NO HAY','NO HAY','NOHAY','tour_default.png' ) ");
                out.print("Tour registrado exitosamente");
                System.out.println("Tour "+nombre+" registrado");
            }
        } catch (SQLException e) {
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
