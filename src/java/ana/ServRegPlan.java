package ana;

import conexion.Conexion;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author anasu
 */
@WebServlet(name = "ServRegPlan", urlPatterns = {"/ServRegPlan"})
public class ServRegPlan extends HttpServlet {

    ResultSet cdr = null;
    Statement sentenciaSQL = null;
    Conexion conecta = new Conexion();

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        conecta.Conectar();
        sentenciaSQL = conecta.getSentenciaSQL();
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServRegPlan</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ServRegPlan at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println(""
                + "<center><p><h2>Registro de Planes</h2></p>"
                + "<input type=text placeholder=Nombre id=nombreP><br/><br/>"
                + "<input type=text placeholder=Costo id=costo><br/><br/>"
                + "<input type=text placeholder=Descripcion id=descripcionP><br/><br/>"
                + "<input type=text placeholder=Duracion id=duracionP><br/><br/>"
                + "<input type=button id=registrar value=Registrar onclick=registPlan2();>"
                + "<br/><br/>"
                + "<div id=detalleP></div>"
                + "</center>");
    }

    public void destroy() {
        conecta.cerrar();
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        try {
            String nombre = request.getParameter("nombreP").toUpperCase();
            double costo = Double.parseDouble(request.getParameter("costo"));
            String descripcion = request.getParameter("descripcionP").toUpperCase();
            String duracion = request.getParameter("duracionP").toUpperCase();
//            if (cdr.next()) {
            sentenciaSQL.executeUpdate("INSERT INTO PLANES VALUES(null,'" + nombre + "'," + costo + ",'" + descripcion + "','" + duracion + "')");
            out.println("El registro se guardo Correctamente");
//            }
        } catch (SQLException ex) {
            out.println("Excepción SQL: " + ex.getMessage());
        } catch (NullPointerException ex) {
            out.println("Apuntando SQL: " + ex.getMessage());
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
