package ana;

import conexion.Conexion;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


/**
 *
 * @author anasu
 */
public class Actualiza {
    private int idplan;
    private double costo;
    private int duracion;
    private String descripcion;
    
    ResultSet cdr = null;
    Statement sentenciaSQL = null;
    Conexion conecta = new Conexion();
    
    public void consulta(int id){
        try {
            Conexion conecta = new Conexion();
            conecta.Conectar();
            String strComando = "SELECT * FROM PLANES WHERE IDPLAN = "+id+" ";
            sentenciaSQL = conecta.getSentenciaSQL();
            cdr = sentenciaSQL.executeQuery(strComando);
            while(cdr.next()){
                id = cdr.getInt(1);
                costo = cdr.getDouble(2);
                duracion = cdr.getInt(4);
                descripcion = cdr.getString(3);
            }
        } catch (SQLException ex) {
            ex.getMessage();
        }
    }

    public int getIdplan() {
        return idplan;
    }

    public double getCosto() {
        return costo;
    }

    public int getDuracion() {
        return duracion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public ResultSet getCdr() {
        return cdr;
    }

    public Statement getSentenciaSQL() {
        return sentenciaSQL;
    }

    public Conexion getConecta() {
        return conecta;
    }

    public void setIdplan(int idplan) {
        this.idplan = idplan;
    }

    public void setCosto(double costo) {
        this.costo = costo;
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setCdr(ResultSet cdr) {
        this.cdr = cdr;
    }

    public void setSentenciaSQL(Statement sentenciaSQL) {
        this.sentenciaSQL = sentenciaSQL;
    }

    public void setConecta(Conexion conecta) {
        this.conecta = conecta;
    }
    

    /**
     * @return the matricula
     */
    
}

