/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.fernanda;

import conexion.Conexion;
import java.sql.ResultSet;
import java.sql.Statement;
import modelo.fernanda.ModeloActivacion;

/**
 *
 * @author Abraham
 */
public class DAOActivarUsuario {
    
    //este metodo va a recibir un objeto de activacion que contiene el id del usuario y el codigo
    public void estadoActivo(ModeloActivacion ac){
     try {
//se definen los stament para realizar la consulta a la base de datos
    ResultSet cdr = null;
    Statement sentenciaSQL = null;
    Conexion conecta = new Conexion();
    conecta.Conectar();
    sentenciaSQL = conecta.getSentenciaSQL();
    //sentencia preparada a la base de datos
    final String sql="update usuario set estado='ACTIVO' where IDUSUARIO="+ac.getId_usuario()+"";
    sentenciaSQL.executeUpdate(sql);
    conecta.cerrar();

     } catch (Exception e) {
            System.out.println("e"+e);
        }
}
    
}
