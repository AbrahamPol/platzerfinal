
package dao.fernanda;

import conexion.Conexion;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.Statement;
import modelo.fernanda.ModeloUsuario;

/**
 *
 * @author Abraham
 */
public class DAORegistroUsuario {
    /*
    este metodo ya teniendo los datos del usuario con las reglas
    de negocio, se procede a guardarlo en la base de datos, 
    con estatus INACTIVO, y con Foto predeterminada,
    Es de suma importancia saber que el estatus cambiara a ACTIVO
    cuando el usuario valla a su correo para activar su cuenta.
    */
    public void RegistroUsuario(ModeloUsuario usuario,PrintWriter out){
        try {
           
            if ( noRepetido(usuario)){
    ResultSet cdr = null;
    Statement sentenciaSQL = null;
    Conexion conecta = new Conexion();
    conecta.Conectar();
    sentenciaSQL = conecta.getSentenciaSQL();
    String sql="INSERT INTO USUARIO VALUES (NULL,'"+usuario.getEmail()+"','"+usuario.getContrasena()+"','"+usuario.getNombre()+"','"+usuario.getPaterno()+"','"+usuario.getFoto()+"','"+usuario.getEstado()+"') ";
    sentenciaSQL.executeUpdate(sql);  
    conecta.cerrar(); 

     // una ves insertado en la base de datos se procede a buscar su id para activar la cuenta en la liga del correo
   usuario.setId(buscarId(usuario));

            }else{
                    out.print("Ya existe una cuenta con ese correo");
            }

        } catch (Exception e) {
            System.out.println("e"+e);
        }
    }
   
    /*
    metodo para no duplicar datos en la base de datos
    */
    public boolean noRepetido(ModeloUsuario usuario){
                try {
    ResultSet cdr = null;
    Statement sentenciaSQL = null;
    Conexion conecta = new Conexion();
    conecta.Conectar();
    sentenciaSQL = conecta.getSentenciaSQL();
    String sql2="select * from usuario where email='"+usuario.getEmail()+"'";
    cdr=sentenciaSQL.executeQuery(sql2);
    int cont=0;
    while (cdr.next()){
        cont++;
    }
    conecta.cerrar();
    if (cont==0){
       return true;
    }else {
        return false;
    }
    

        } catch (Exception e) {
            System.out.println("e"+e);
        }
       return false;
    }
    
    
     public int buscarId(ModeloUsuario usuario){
                try {
    ResultSet cdr = null;
    Statement sentenciaSQL = null;
    Conexion conecta = new Conexion();
    conecta.Conectar();
    sentenciaSQL = conecta.getSentenciaSQL();
    String sql2="select * from usuario where email='"+usuario.getEmail()+"' and estado='INACTIVO'";
    cdr=sentenciaSQL.executeQuery(sql2);
    int cont=0;
    while (cdr.next()){
        return cdr.getInt("IDUSUARIO");
    }
    conecta.cerrar();
 
        } catch (Exception e) {
            System.out.println("e"+e);
        }
       return 0;
    }
    
}
