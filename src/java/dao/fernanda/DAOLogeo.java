
package dao.fernanda;

import conexion.Conexion;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import modelo.fernanda.ModeloLogeo;

/**
 *
 * @author Abraham
 */
public class DAOLogeo {
    
    public void inicioSesion(ModeloLogeo modelo,PrintWriter out,HttpServletRequest request){
        //declaracion de variables para contener informacion, y validar
        int cont=0;
        String emailBase=""; 
        String passwordBase="";
        String estadoBase="";
        String idUsuarioBase="";
        String nombreBase="";
        String paternoBase="";
        try{
    //consulta a la base de datos
    final String sql = " SELECT * FROM USUARIO WHERE email='"+modelo.getEmail()+"' ";
          
//se definen los stament para realizar la consulta a la base de datos
    ResultSet cdr ;
    Statement sentenciaSQL =null ;
    Conexion conecta = new Conexion();
    conecta.Conectar();
    sentenciaSQL = conecta.getSentenciaSQL();
    cdr=sentenciaSQL.executeQuery(sql); 
    //consulta de registros
        while(cdr.next()) {
            cont++;
        emailBase= cdr.getString("email");
        passwordBase= cdr.getString("contrasena");
        estadoBase=cdr.getString("estado");
        idUsuarioBase=cdr.getString("IDUSUARIO");
        nombreBase=cdr.getString("NOMBRE");
        paternoBase=cdr.getString("APATERNO");
        }//while
    ///////////////////////////////////////////////////
   
    /*
    1.-PRIMER VALIDACION, SABER SI EXISTEN REALMENTE EN LA BASE DE DATOS
    LOS DATOS QUE ESTOY RECIBIENDO.
    2.-SEGUNDA VALIDACION, SABER SI EL CORREO Y CONTRASEÑA QUE RECIBO ESTA ACTIVO
    3.-TERCERA VALIDACION, comprobar que el correo exista, y la contraseña sea diferente(este activo o inactivo no importa)
    4.-CUARTA VALIDACION,SI LOS DATOS SON IGUALES A LA BASE DE DATOS SE INICIA SESION
    5.- QUINTA VALIDACION, SI LOS DATOS SON IGUALES A LA BASE DE DATOS PERO SE DIO DE BAJA
    AUTOMATICAMENTE SE INICIA SESSION NORMAL, SIN EMBARGO SE DEBE CAMBIAR SU ESTATUS A "ACTIVO"
    ESTO ES SIMILAR A FACEBOOK
    */
    if (cont==0){
        out.print("2");//usuario inexistente
    }else if (modelo.getEmail().equalsIgnoreCase(emailBase) && modelo.getContrasena().equalsIgnoreCase(passwordBase) && estadoBase.equals("INACTIVO")){
        out.print("1");//debes de activar tu cuenta, ve a tu bandeja de entrada de tu correo y haz click en la liga para activar tu cuenta.
    }else if (modelo.getEmail().equalsIgnoreCase(emailBase) && !modelo.getContrasena().equals(passwordBase)){
        out.print("3"); //usuario o contraseña incorrectas
    }else if (modelo.getEmail().equalsIgnoreCase(emailBase) && modelo.getContrasena().equalsIgnoreCase(passwordBase) && estadoBase.equals("ACTIVO")){
         HttpSession usuario = request.getSession();
         usuario.setAttribute("id_usuario", idUsuarioBase);
         usuario.setAttribute("nombre", nombreBase);
         usuario.setAttribute("apellido", paternoBase); 
         out.print("Platzer.jsp");
    }else if(modelo.getEmail().equalsIgnoreCase(emailBase) && modelo.getContrasena().equalsIgnoreCase(passwordBase) && estadoBase.equals("BAJA")){
        activarEstado(modelo,out);
        HttpSession usuario = request.getSession();
         usuario.setAttribute("id_usuario", idUsuarioBase);
         usuario.setAttribute("nombre", nombreBase);
         usuario.setAttribute("apellido", paternoBase); 
        out.print("Platzer.jsp");
    }
    conecta.cerrar();
} catch (Exception e3) {
System.out.println("error de dao"+e3);
} 

    }//cierre de metodo

    public void activarEstado(ModeloLogeo modelo, PrintWriter out) {
 /*
    Metodo para cambiar el estado del usuario a activo, despues de haber dado de baja su cuenta
        */
        try {
    final String sql="update usuario set estado='ACTIVO' where email='"+modelo.getEmail()+"' and contrasena='"+modelo.getContrasena()+"'";
    Statement sentenciaSQL =null ;
    Conexion conecta = new Conexion();
    conecta.Conectar();
    sentenciaSQL = conecta.getSentenciaSQL();
    sentenciaSQL.executeUpdate(sql); 
    conecta.cerrar();
    
        } catch (Exception e) {
            System.out.println(""+e);
        }
    
    }

}