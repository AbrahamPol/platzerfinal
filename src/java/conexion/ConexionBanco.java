
package conexion;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.BreakIterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hernandez
 */
public class ConexionBanco {
    
    private Connection conexionBanco = null;
    private Statement sentenciaSQLB = null;
    
    public void ConectarB(){
        try {
            String controlador = "com.mysql.jdbc.Driver";
            Class.forName(controlador).newInstance();
            conexionBanco = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/BANCO","root","");
            sentenciaSQLB = getConexionB().createStatement();
        } catch (InstantiationException ex) {
            System.out.println("Objeto no creado: " + ex.getMessage());
        } catch (IllegalAccessException ex) {
            System.out.println("Acceso Ilegal: " + ex.getMessage());
        } catch (ClassNotFoundException ex) {
            System.out.println("No se pudo cargar el controlador: " + ex.getMessage());
        } catch (SQLException ex) {
            System.out.println("Excepción SQL: " + ex.getMessage());
        }
    }
    
    public void cerrar(){
        try{
            if(getSentenciaSQLB() != null)
            getSentenciaSQLB().close();
        if(getConexionB() != null)
            getConexionB().close();
        } catch(SQLException ignorada){}
    }

    public Connection getConexionB() {
        return conexionBanco;
    }

    public Statement getSentenciaSQLB() {
        return sentenciaSQLB;
    }
    
    
}
