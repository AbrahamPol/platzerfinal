
package conexion;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.BreakIterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hernandez
 */
public class Conexion {
    
    private Connection conexion = null;
    private Statement sentenciaSQL = null;
    
    public void Conectar(){
        try {
            String controlador = "com.mysql.jdbc.Driver";
            Class.forName(controlador).newInstance();
            conexion = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/platzer","root","");
            sentenciaSQL = getConexion().createStatement();
        } catch (InstantiationException ex) {
            System.out.println("Objeto no creado: " + ex.getMessage());
        } catch (IllegalAccessException ex) {
            System.out.println("Acceso Ilegal: " + ex.getMessage());
        } catch (ClassNotFoundException ex) {
            System.out.println("No se pudo cargar el controlador: " + ex.getMessage());
        } catch (SQLException ex) {
            System.out.println("Excepción SQL: " + ex.getMessage());
        }
    }
    
    public void cerrar(){
        try{
            if(getSentenciaSQL() != null)
            getSentenciaSQL().close();
        if(getConexion() != null)
            getConexion().close();
        } catch(SQLException ignorada){}
    }

    public Connection getConexion() {
        return conexion;
    }

    public Statement getSentenciaSQL() {
        return sentenciaSQL;
    }
    
    
}
