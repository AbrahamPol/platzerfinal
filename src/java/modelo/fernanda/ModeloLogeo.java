
package modelo.fernanda;

/**
 *
 * @author Abraham
 */
public class ModeloLogeo {
    
    /*
    metodos de acceso para iniciar session
    */
   private String email;
   private String contrasena;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public ModeloLogeo(String email, String contrasena) {
        this.email = email;
        this.contrasena = contrasena;
    }
   
   public ModeloLogeo()
   {
       
   }

    @Override
    public String toString() {
        return "ModeloLogeo{" + "email=" + email + ", contrasena=" + contrasena + '}';
    }

   
    
}
