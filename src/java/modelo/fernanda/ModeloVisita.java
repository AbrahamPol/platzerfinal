
package modelo.fernanda;

/**
 *
 * @author Abraham
 */
public class ModeloVisita {
    //metodos de acceso para visita
//mysql> desc visita;
//+--------------+---------+------+-----+---------+----------------+
//| Field        | Type    | Null | Key | Default | Extra          |
//+--------------+---------+------+-----+---------+----------------+
//| IDVISITA     | int(11) | NO   | PRI | NULL    | auto_increment |
//| IDLUGAR      | int(11) | YES  |     | NULL    |                |
//| FECHA        | text    | YES  |     | NULL    |                |
//| COSTO        | double  | YES  |     | NULL    |                |
//| DESCRIPCION  | text    | YES  |     | NULL    |                |
//| CALIFICACION | int(11) | YES  |     | NULL    |                |
//+--------------+---------+------+-----+---------+----------------+
//6 rows in set (0.01 sec)
    
private String nombre_lugar;
private String id_lugar;
private String Fecha;
private float costo;
private String Descripcion;
private int calificacion;
private String imagen;

    public String getNombre_lugar() {
        return nombre_lugar;
    }

    public void setNombre_lugar(String nombre_lugar) {
        this.nombre_lugar = nombre_lugar;
    }

    public String getId_lugar() {
        return id_lugar;
    }

    public void setId_lugar(String id_lugar) {
        this.id_lugar = id_lugar;
    }

    public String getFecha() {
        return Fecha;
    }

    public void setFecha(String Fecha) {
        this.Fecha = Fecha;
    }

    public float getCosto() {
        return costo;
    }

    public void setCosto(float costo) {
        this.costo = costo;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public int getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(int calificacion) {
        this.calificacion = calificacion;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    
    
    
    
    
    
    public ModeloVisita(String nombre_lugar, String Fecha, float costo, String Descripcion, int calificacion,String imagen) {
        this.nombre_lugar = nombre_lugar;
        this.Fecha = Fecha;
        this.costo = costo;
        this.Descripcion = Descripcion;
        this.calificacion = calificacion;
        this.imagen=imagen;
    }

  public ModeloVisita(){
      
  }

}
