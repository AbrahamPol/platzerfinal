
package modelo.fernanda;

/**
 *
 * @author Abraham
 */
public class ModeloUsuario {
    /*
    Cabe mencionar que esta clase solo se utilizara cuando el usuario,
    se registre por primera vez a PLATZER.
    */
    //creacion de metodos de acceso, e inicializador de constructor, de acuerdo a tabla usuario

//+------------+---------+------+-----+---------+----------------+
//| Field      | Type    | Null | Key | Default | Extra          |
//+------------+---------+------+-----+---------+----------------+
//| IDUSUARIO  | int(11) | NO   | PRI | NULL    | auto_increment |
//| EMAIL      | text    | YES  |     | NULL    |                |
//| CONTRASENA | text    | YES  |     | NULL    |                |
//| NOMBRE     | text    | YES  |     | NULL    |                |
//| APATERNO   | text    | YES  |     | NULL    |                |
//| FOTO       | text    | YES  |     | NULL    |                |
//| ESTADO     | text    | YES  |     | NULL    |                |
//+------------+---------+------+-----+---------+----------------+

private int id;
private String email;
private String contrasena;
private String nombre;
private String paterno;



/*
Aqui guardare la url de la imagen , ya que la imagen verdadera la voy a guardar en una carpeta
 de img/fer.
Es mejor guardar la ruta de la imagen, para cuando se necesita se busque en la carpeta
de imgFer,de esta manera en HTML:
<img src="archivo.jpg">
*/
private String foto;
/*
Para estado se manejan dos casos
1.-ACTIVO
2.-INACTIVO
*/
private String estado;

  
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPaterno() {
        return paterno;
    }

    public void setPaterno(String paterno) {
        this.paterno = paterno;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    
    
    public ModeloUsuario(String email, String contrasena, String nombre, String paterno) {
        this.id=0;
        this.email = email;
        this.contrasena = contrasena;
        this.nombre = nombre;
        this.paterno = paterno;
        /*
        Aqui mandare la url de una imagen predeterminada, para que despues el usuario la cambie
        */
        this.foto = "../imgFer/1.png";
        /*
        Aqui lo pongo inactivo, por que cuando se registre el por primera vez el usuario;
        forzosamente necesitara ir a la liga de su email, ya que di click a la liga
        el estatus cambiara a ACTIVO
        */
        this.estado = "INACTIVO";
    }

//constructor por defecto
    public ModeloUsuario(){
    }

}
