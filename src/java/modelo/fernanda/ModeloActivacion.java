
package modelo.fernanda;

/**
 *
 * @author Abraham
 */
public class ModeloActivacion {
    private String id_usuario;
    private String codigo;

    public String getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(String id_usuario) {
        this.id_usuario = id_usuario;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public ModeloActivacion(String id_usuario, String codigo) {
        this.id_usuario = id_usuario;
        this.codigo = codigo;
    }

    
  
    public ModeloActivacion(){
        
    }
            
}
