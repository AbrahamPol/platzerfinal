
package modelo.fernanda;

/**
 *
 * @author Abraham
 */
public class ModeloLugar {
//    +---------+---------+------+-----+---------+----------------+
//| Field   | Type    | Null | Key | Default | Extra          |
//+---------+---------+------+-----+---------+----------------+
//| IDLUGAR | int(11) | NO   | PRI | NULL    | auto_increment |
//| NOMBRE  | text    | YES  |     | NULL    |                |
//| PAIS    | text    | YES  |     | NULL    |                |
//| ESTADO  | text    | YES  |     | NULL    |                |
//+---------+---------+------+-----+---------+----------------+
    //creacion de metodos de acceso
    private String nombre;
    private String descripcion;
    private String foto;

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public ModeloLugar(String nombre, String descripcion, String foto) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.foto = foto;
    }

    


    
public ModeloLugar(){
    
}

    

}
