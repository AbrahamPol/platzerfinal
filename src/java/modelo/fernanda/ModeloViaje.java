
package modelo.fernanda;

/**
 *
 * @author Abraham
 */
public class ModeloViaje {
    
//    mysql> desc viaje;
//+-----------+---------+------+-----+---------+----------------+
//| Field     | Type    | Null | Key | Default | Extra          |
//+-----------+---------+------+-----+---------+----------------+
//| IDVIAJE   | int(11) | NO   | PRI | NULL    | auto_increment |
//| IDLUGAR   | int(11) | YES  | MUL | NULL    |                |
//| IDUSUARIO | int(11) | YES  | MUL | NULL    |                |
//| fecha     | text    | YES  |     | NULL    |                |
//+-----------+---------+------+-----+---------+----------------+
    
    private int id_viaje ;
    private int id_lugar ;
    private int id_usuario ;
    private String fecha;
    private String nombreLugar;

    public String getNombreLugar() {
        return nombreLugar;
    }

    public void setNombreLugar(String nombreLugar) {
        this.nombreLugar = nombreLugar;
    }
            

    public int getId_viaje() {
        return id_viaje;
    }

    public void setId_viaje(int id_viaje) {
        this.id_viaje = id_viaje;
    }

    public int getId_lugar() {
        return id_lugar;
    }

    public void setId_lugar(int id_lugar) {
        this.id_lugar = id_lugar;
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public ModeloViaje(int id_usuario, String fecha, String nombreLugar) {
        this.id_usuario = id_usuario;
        this.fecha = fecha;
        this.nombreLugar = nombreLugar;
    }

   

public ModeloViaje(){
    
}

    @Override
    public String toString() {
        return "ModeloViaje{" + "id_viaje=" + id_viaje + ", id_lugar=" + id_lugar + ", id_usuario=" + id_usuario + ", fecha=" + fecha + ", nombreLugar=" + nombreLugar + '}';
    }




}