DROP SCHEMA IF EXISTS BANCO;
CREATE SCHEMA BANCO;
USE BANCO

DROP TABLE IF EXISTS TARJETA;
CREATE TABLE TARJETA (
IDTARJETA INT AUTO_INCREMENT PRIMARY KEY,
NOTARJETA TEXT,
TITULAR TEXT,
MESEXPIRACION TEXT,
ANOEXPIRACION TEXT,
CODIGOSEGURIDAD TEXT,
SALDO DOUBLE) ENGINE = INNODB;

INSERT INTO TARJETA VALUES(NULL,"1234567890123456","FERNANDA GONZALEZ","01","2023","630",1500);
INSERT INTO TARJETA VALUES(NULL,"2234567890123456","JOSE HERNANDEZ","02","2023","631",22000);
INSERT INTO TARJETA VALUES(NULL,"3234567890123456","ANA SUAREZ","03","2023","632",350);
INSERT INTO TARJETA VALUES(NULL,"4234567890123456","JAVIER TORRES","04","2023","633",6500);
INSERT INTO TARJETA VALUES(NULL,"5234567890123456","EDUARDO FIGUEROA","05","2023","634",12000);
INSERT INTO TARJETA VALUES(NULL,"6234567890123456","ERIK MARTINEZ","06","2023","635",150);

INSERT INTO TARJETA VALUES(NULL,"7234567890123456","JOSE HERNANDEZ","07","2023","636");
INSERT INTO TARJETA VALUES(NULL,"8234567890123456","JOSE HERNANDEZ","08","2023","637");
INSERT INTO TARJETA VALUES(NULL,"9234567890123456","JOSE HERNANDEZ","09","2023","638");
INSERT INTO TARJETA VALUES(NULL,"0234567890123456","JOSE HERNANDEZ","10","2023","639");
INSERT INTO TARJETA VALUES(NULL,"1234567890123456","JOSE HERNANDEZ","11","2023","640");
