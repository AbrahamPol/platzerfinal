<%-- 
    Document   : index.jsp
    Created on : 28/09/2018, 10:35:35 PM
    Author     : Hernandez
--%>

<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="conexion.Conexion" %>
<%@page import="java.io.PrintWriter" %>
<%@page import="java.sql.ResultSet" %>
<%@page import="java.sql.SQLException" %>
<%@page import="java.sql.Statement" %>
<!DOCTYPE html>
<html>
    <head>
        
	    <!-- ==============================================
		Title and Meta Tags
		=============================================== -->
		<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">  
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>¡Bienvenido! - Platzer</title>
		<meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta property="og:title" content="" />
        <meta property="og:url" content="" />
        <meta property="og:description" content="" />		
		
		<!-- ==============================================
		Favicons
		=============================================== --> 
		<link rel="icon" href="assets/img/logo.jpg">
		<link rel="apple-touch-icon" href="img/favicons/apple-touch-icon.png">
		<link rel="apple-touch-icon" sizes="72x72" href="img/favicons/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="114x114" href="img/favicons/apple-touch-icon-114x114.png">
		
	    <!-- ==============================================
		CSS
		=============================================== -->
        <link type="text/css" href="assets/css/demos/photo.css" rel="stylesheet" />
        <link type="text/css" href="assets/css/demos/interest.css" rel="stylesheet" />
        <link type="text/css" href="assets/css/skins/skin_two.css" rel="stylesheet" />                        
				
		<!-- ==============================================
		Feauture Detection
		=============================================== -->
		<script src="assets/js/modernizr-custom.js"></script>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->	
		  <!-- ==============================================
		JS FERNANDA
		=============================================== -->
                  <script type="text/javascript" src="../jsFernanda/7RegistroUsuario.js"></script> 
                  <script type="text/javascript" src="../jsFernanda/4Logeo.js"></script>  
  </head>
    <body>         
                                       
               
                        
                         
                        <script type="text/javascript" src="scriptPlatzer.js"></script>            
        <table width="100%" border="1">
            <tr>
                <td>
                    <!-- ==============================================
     Navigation Section
     =============================================== -->  
     <header class="tr-header">
      <nav class="navbar navbar-default">
       <div class="container-fluid">
	    <div class="navbar-header">
		 <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
		  <span class="sr-only">Toggle navigation</span>
		  <span class="icon-bar"></span>
		  <span class="icon-bar"></span>
		  <span class="icon-bar"></span>
		 </button>
		 <a class="navbar-brand" href="index.jsp"> Platzer</a>
		</div><!-- /.navbar-header -->
		<div class="navbar-left">
		 <div class="collapse navbar-collapse" id="navbar-collapse">
		  <ul class="nav navbar-nav">
		  </ul>
		 </div>
		</div><!-- /.navbar-left -->
		<div class="navbar-right">                          
		 <ul class="nav navbar-nav">
                  <li><i class="fa fa-home"></i></li>
		  <li><a href="#"  onclick="DinamicoDiv('inicio');">Inicio </a></li>
                  <li><i class="fa fa-align-left"></i></li>
		  <li><a href="#" onclick="DinamicoDiv('empresa');">Iniciar | Registrar </a></li>
		  <li><i class="fa fa-user"></i></li>
		  <li><a href="#" onclick="DinamicoDiv('miembro');">Iniciar | Registrate </a></li>                  
		 </ul><!-- /.sign-in -->   
		</div><!-- /.nav-right -->
       </div><!-- /.container -->
      </nav><!-- /.navbar -->
     </header><!-- Page Header --> 
                </td>                                               
            </tr>        
        </table>
        
        <table width="100%">
            <tr>
                <td colspan="5">
                    <div id="inicio" align="center">
                        <!-- ==============================================
	 INICIO
	 =============================================== -->
     <section class="landing_page">
      <div class="container">
       <div class="banner-content text-center">
        <h1>Platzer</h1>
        <h2>Social networks reimagined..as a collection of interest-specific social channels like on your cable TV.</h2><br/>
		
       </div><!--/. banner-content -->
      </div><!-- /.container -->
     </section>  
	 
	 <!-- ==============================================
	 Fluffs Section
	 =============================================== -->
     <section class="fluffs" id="fluffs">
      <div class="container">
	  
	   <div class="row">
	    <div class="section-title-pacifico">
	     <h1>Passions & Interests</h1>
	    </div>	   
	   </div>
	  
	   <div class="row">
	   
	    <div class="col-md-3 col-sm-6 col-xs-12">
         <div class="photo-rounded-fluffs">
		  <a href="#"> 
           <img class="img-responsive" src="assets/img/fluffs/1.jpg" alt="Image">
		   <h1 class="text-center">Moments</h1>
		  </a> 
         </div>
		</div> 
		
		<div class="col-md-3 col-sm-6 col-xs-12">
         <div class="photo-rounded-fluffs">
		  <a href="#">
           <img class="img-responsive" src="assets/img/fluffs/2.jpg" alt="Image">
		   <h1 class="text-center">Comedy</h1>
		  </a> 
         </div>
		</div>
		
		<div class="col-md-3 col-sm-6 col-xs-12">
         <div class="photo-rounded-fluffs">
		  <a href="#"> 
           <img class="img-responsive" src="assets/img/fluffs/3.jpg" alt="Image">
		   <h1 class="text-center">Womanly</h1>
		  </a> 
         </div>
		</div>
		
		<div class="col-md-3 col-sm-6 col-xs-12">
         <div class="photo-rounded-fluffs">
		  <a href="#">
           <img class="img-responsive" src="assets/img/fluffs/4.jpg" alt="Image">
		   <h1 class="text-center">Manly</h1>
		  </a> 
         </div>
		</div>
		
	   </div>
	   
	   <div class="row">
	   
	    <div class="col-md-3 col-sm-6 col-xs-12">
         <div class="photo-rounded-fluffs">
		  <a href="#">
           <img class="img-responsive" src="assets/img/fluffs/5.jpg" alt="Image">
		   <h1 class="text-center">Movies</h1>
		  </a> 
         </div>
		</div> 
		
		<div class="col-md-3 col-sm-6 col-xs-12">
         <div class="photo-rounded-fluffs">
		  <a href="#">
           <img class="img-responsive" src="assets/img/fluffs/6.jpg" alt="Image">
		   <h1 class="text-center">Music</h1>
		  </a> 
         </div>
		</div>
		
		<div class="col-md-3 col-sm-6 col-xs-12">
         <div class="photo-rounded-fluffs">
		  <a href="#">
           <img class="img-responsive" src="assets/img/fluffs/7.jpg" alt="Image">
		   <h1 class="text-center">Celebs</h1>
		  </a> 
         </div>
		</div>
		
		<div class="col-md-3 col-sm-6 col-xs-12">
         <div class="photo-rounded-fluffs">
		  <a href="#">
           <img class="img-responsive" src="assets/img/fluffs/11.jpg" alt="Image">
		   <h1 class="text-center">Politics</h1>
		  </a> 
         </div>
		</div>
		
	   </div>
	   
	   <div class="row">
		
		<div class="col-md-3 col-sm-6 col-xs-12">
         <div class="photo-rounded-fluffs">
		  <a href="#">
           <img class="img-responsive" src="assets/img/fluffs/8.jpg" alt="Image">
		   <h1 class="text-center">Sports</h1>
		  </a> 
         </div>
		</div>
	   
	    <div class="col-md-3 col-sm-6 col-xs-12">
         <div class="photo-rounded-fluffs">
		  <a href="#">
           <img class="img-responsive" src="assets/img/fluffs/9.jpg" alt="Image">
		   <h1 class="text-center">Technology</h1>
		  </a> 
         </div>
		</div> 
		
		<div class="col-md-3 col-sm-6 col-xs-12">
         <div class="photo-rounded-fluffs">
		  <a href="#">
           <img class="img-responsive" src="assets/img/fluffs/10.jpg" alt="Image">
		   <h1 class="text-center">Business</h1>
		  </a> 
         </div>
		</div>
		
		<div class="col-md-3 col-sm-6 col-xs-12">
         <div class="photo-rounded-fluffs">
		  <a href="#">
           <img class="img-responsive" src="assets/img/fluffs/12.jpg" alt="Image">
		   <h1 class="text-center">Games</h1>
		  </a> 
         </div>
		</div>
		
	   </div>
	   
	  </div>
	 </section>	 
	 
     <!-- ==============================================
     PIE DE PAGINA
     =============================================== -->	
     <footer id="colophon" class="site-footer">
      <div id="footer-menu">
	   <ul>
	    <li><a href="#">About</a></li>
		<li><a href="#">How it works</a></li>
		<li><a href="#">Contact</a>  </li>
	   </ul>
	   <div class="footer-social-icons">
        <div class="social-icons">
		 <a href="#"><i class="fab fa-instagram"></i></a>
		 <a href="#"><i class="fab fa-facebook"></i></a>
		 <a href="#"><i class="fab fa-twitter"></i></a>
		</div>  
       </div>
	   <p class="footer-copyright">© 2018 PLatzer Inc.</p>
	  </div>
     </footer>	 
	 
	 
     <!-- ==============================================
	 Scripts
	 =============================================== -->
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/base.js"></script>
                    </div>  
                    <%
            if(request.getParameter("loginem") != null){
                
                try {
                    String email = request.getParameter("usernameem");
                    String password = request.getParameter("passwordem");
                                Class.forName("com.mysql.jdbc.Driver").newInstance();
                                Connection CON = java.sql.DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/PLATZER","root","");

                                String qrCate = " SELECT * FROM EMPRESA WHERE EMAIL = '"+email+"' ";
                                PreparedStatement PS = CON.prepareStatement(qrCate);
                                ResultSet RS = PS.executeQuery();

                                while(RS.next()) {
                                    String emailcdr = RS.getString(6);
                                    String passwordcdr = RS.getString(8);
                                    if(email.equals(emailcdr) && password.equals(passwordcdr)){ 
                                        HttpSession usuario = request.getSession();
                                        usuario.setAttribute("nombre", RS.getString(2));                                      
                                        response.sendRedirect("PlatzerEnterprise.jsp");
                                    }
                                    else{
                                        out.println("<script>location.replace('index.jsp');</script>");                    
                                    }
                                } 
                                System.out.println("Conexion realizada");
                            } catch (Exception e3) {
                                
                                System.out.println("No hizo conexion");
                                e3.printStackTrace(System.err);
                            } 
                                
                            }
            
                            
                        %>
                    
                    <div id="empresa" style="display: none">
                        <!-- ==============================================
     EMPRESA
     =============================================== -->  
     <section class="home" id="home">
      <div class="container">
   	   <div class="row">
	   
        <div class="col-lg-4 num-down">	
            <div class="logo col-lg-12">
		  <a href="home.html"><h2>Platzer Enterprise</h2></a><br/>
		  <p>Discover a world where people share your passions & interests.</p>
		 </div>		
		 <div class="logo num col-lg-12">
		  <a><h2>Platzers Registrados</h2></a><br/>
		  <p>1,200,000,000</p>
		 </div>		
        </div>  
		
        <div class="col-lg-4 col-lg-offset-4 col-md-12 col-sm-12 col-xs-12">
      	 <form class="form-login" method="post" id="loginform">
          <div class="login-wrap">
           <div class="form-group">
		    <input name="usernameem" type="text" class="form-control" placeholder="Email">
		   </div>
		  <div class="form-group">
           <input type="password" class="form-control" name="passwordem" placeholder="Contraseña">
          </div>
          <button class="btn btn-theme btn-block" type="submit" name="loginem"><i class="fa fa-lock"></i> Iniciar Sesión</button>
          <label class="checkbox text-center">
          
          </label>
         </div>
        </form>
        <br/> 
        
		<form class="form-login"  mehotd="post" id="">
		 <h4 class="form-login-heading">Registra tu Empresa</h4>
		 <div class="login-wrap">
                     <div class="form-group">
		   <input id="nombreE" type="text" class="form-control" placeholder="Nombre">
		  </div>
		  <div class="">
                      Hotel<br/><input id="1" name="r" value="Hotel" type="radio" class="" placeholder="">
		  </div>                     
                   <div class="">
                       Agencia<br/><input id="2" name="r" value="Agencia" type="radio" class="" placeholder="">
		  </div>
		  <div class="form-group">
		   <input type="text" class="form-control" id="direccionE" placeholder="Direccion">
		  </div>
                  <div class="form-group">
		   <input type="text" class="form-control" id="telefonoE" placeholder="Telefono">
		  </div>
                     <div class="form-group">
		   <input type="text" class="form-control" id="emailE" placeholder="Email">
		  </div>
		  <div class="form-group">
		   <input type="password" class="form-control" id="passwordE" placeholder="Password">
		  </div>
                     <button class="btn btn-theme btn-block" name="register" onclick=" registrarEmpre();" type="submit"><i class="fa fa-lock"></i> Registrar</button>
	     </div> 	
		 </form>
		 
		</div> 
		 
	   </div><!-- End row-->
      </div><!-- End container--> 
     </section><!-- End section-->	
	 
	 
     <!-- ==============================================
	 Scripts
	 =============================================== -->
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/base.js"></script>


                    </div>
                    
           <div id="miembro" style="display: none">
                        
     <!-- ==============================================
     MIEMBRO
     =============================================== -->  
     <section class="home" id="home">
      <div class="container">
   	   <div class="row">
	   
        <div class="col-lg-4 num-down">	
            <div class="logo col-lg-12">
		  <a href="home.html"><h2>Platzer</h2></a><br/>
		  <p>Discover a world where people share your passions & interests.</p>
		 </div>		
		 <div class="logo num col-lg-12">
		  <a><h2>Platzers Registrados</h2></a><br/>
		  <p>1,200,000,000</p>
		 </div>		
        </div> 
		
        <div class="col-lg-4 col-lg-offset-4 col-md-12 col-sm-12 col-xs-12">
          
          <div class="login-wrap">
           <div class="form-group">
		    <input id="emailLogeo" type="text" class="form-control" placeholder="Email">
		   </div>
		  <div class="form-group">
           <input type="password" class="form-control" id="passwordLogeo" placeholder="Contraseña">
          </div>
              <button class="btn btn-theme btn-block" type="button" name="loginu" onclick="peticionLogeo();"><i class="fa fa-lock"></i> Iniciar Sesión</button>
          <label class="checkbox text-center">
          
          </label>
         </div>
        <br/> 
        <br><br><br>
           <div id="RespuestasMiembro">
                
               </div>
    
        
		 <h4 class="form-login-heading">Registrate como miembro</h4>
		 <div class="login-wrap">
		  <div class="form-group">
                      <input name="emailu"  id="emailu" type="email" class="form-control" placeholder="Email" required>
		  </div>
		  <div class="form-group">
                      <input type="password" class="form-control" name="passwordu" id="passwordu" placeholder="Contraseña" required>
		  </div>
		  <div class="form-group">
                      <input type="text" class="form-control" name="nombreu" id="nombreu" onkeypress="return soloLetras(event);" placeholder="Nombre" required>
		  </div>
		  <div class="form-group">
                      <input type="text" class="form-control" name="apaterno" id="apaterno" placeholder="Apellido" onkeypress="return soloLetras(event);" required>
		  </div>
                     <button class="btn btn-theme btn-block" name="registraru"   onclick="registrarUsuarioF();"><i class="fa fa-lock"></i> Registrar</button><br/>                     
	     </div> 	
		
         
	   </div><!-- 
		</div> 
              End row-->
      </div><!-- End container--> 
     </section><!-- End section-->	
	 
	 
     <!-- ==============================================
	 Scripts
	 =============================================== -->
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/base.js"></script>

                    </div>                                                            
                </td>
            </tr>
                        
        </table>
    </body>
</html>
