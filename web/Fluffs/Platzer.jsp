<%-- 
    Document   : Platzer
    Created on : 10/09/2011, 12:22:25 PM
    Author     : alumno
--%>

<%@page import="conexion.Conexion"%>
<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Expires" content="0">

        <meta http-equiv="Last-Modified" content="0">

        <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">

        <meta http-equiv="Pragma" content="no-cache">
        <% HttpSession usuario = request.getSession(); %>
        	    <!-- ==============================================
		Title and Meta Tags
		=============================================== -->
		<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">  
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Platzer</title>
		<meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta property="og:title" content="" />
        <meta property="og:url" content="" />
        <meta property="og:description" content="" />		
		
		<!-- ==============================================
		Favicons
		=============================================== --> 
		<link rel="icon" href="assets/img/logo.jpg">
		<link rel="apple-touch-icon" href="img/favicons/apple-touch-icon.png">
		<link rel="apple-touch-icon" sizes="72x72" href="img/favicons/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="114x114" href="img/favicons/apple-touch-icon-114x114.png">
		
	    <!-- ==============================================
		CSS
		=============================================== -->
        <link type="text/css" href="assets/css/demos/photo.css" rel="stylesheet" />
        <link type="text/css" href="assets/css/demos/interest.css" rel="stylesheet" />
        <link type="text/css" href="assets/css/skins/skin_two.css" rel="stylesheet" />
        <script src="../jsFernanda/2EditarDatos.js"></script>	
        <script src="../jsFernanda/1CRUDVIAJES.js"></script>	
        <script type="text/javascript" src="../scriptPlatzer.js"></script>         
        <script src="../jsFernanda/1CIERREYCUENTA.js"></script>	
        <script src="../jsFernanda/1AgregarFavoritos.js"></script>	
        <script src="../jsFernanda/4RegistroLugar.js"></script>	
        <script src="../jsFernanda/7Visitas.js"></script>
        <script src="../jsFernanda/2CambiarFoto.js"></script>
        <script src="../jsFernanda/1Publicaciones.js"></script>
         <script src="../jsFernanda/1FotosPerfil.js"></script>
        <script src="../jsFernanda/1PublicacionesPerfil.js"></script>
        <script src="../jsFernanda/1BusquedaPublicacion.js"></script>
		<!-- ==============================================
		Feauture Detection
		=============================================== -->
		<script src="assets/js/modernizr-custom.js"></script>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
                <style>
                    #form {
  width: 250px;
  margin: 0 auto;
  height: 50px;
}

#form p {
  text-align: center;
}

#form label {
  font-size: 20px;
}

input[type="radio"] {
  display: none;
}

label {
  color: grey;
}

.clasificacion {
  direction: rtl;
  unicode-bidi: bidi-override;
}

label:hover,
label:hover ~ label {
  color: orange;
}

input[type="radio"]:checked ~ label {
  color: orange;
}
                    
                </style>
    </head>
    <body>
        <script type="text/javascript" src="scriptPlatzer.js"></script>
        
                            <!-- ==============================================
     Navigation Section
     =============================================== -->  
     <header class="tr-header">
      <nav class="navbar navbar-default">
       <div class="container-fluid">
	    <div class="navbar-header">
		 <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
		  <span class="sr-only">Toggle navigation</span>
		  <span class="icon-bar"></span>
		  <span class="icon-bar"></span>
		  <span class="icon-bar"></span>
		 </button>
		 <a class="navbar-brand" href="Platzer.jsp">Platzer</a>
		</div><!-- /.navbar-header -->
		<div class="navbar-left">
		 <div class="collapse navbar-collapse" id="navbar-collapse">
		  <ul class="nav navbar-nav">
		  </ul>
		 </div>
		</div><!-- /.navbar-left -->
		<div class="navbar-right">                          
		 <ul class="nav navbar-nav">
		   <li>
		   <div class="search-dashboard">
               <form>
                    <input placeholder="Buscar" type="text" id="nombreBusqueda">
                    <button type="button" onclick="DinamicoDiv('inicio'); mostrarComentarios(); peticionPublicacionBusqueda(); "><i class="fa fa-search"></i></button>
               </form>
          </div>							
		   </li>

		   <li class="dropdown notification-list">
		    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
			 <i class="fa fa-bell noti-icon"></i>
			 <span class="badge badge-danger badge-pill noti-icon-badge">4</span>
			</a>
			<div class="dropdown-menu dropdown-menu-right dropdown-lg">
             
			 <div class="dropdown-item noti-title">
			  <h6 class="m-0">
			   <span class="pull-right">
			    <a href="" class="text-dark"><small>Clear All</small></a> 
			   </span>Notification
			  </h6>
			 </div>

			 <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 416.983px;">
			  <div class="slimscroll" style="max-height: 230px; overflow: hidden; width: auto; height: 416.983px;">
			   <div id="Slim">
			    <a href="javascript:void(0);" class="dropdown-item notify-item">
				 <div class="notify-icon bg-success"><i class="fa fa-comment"></i></div>
				 <p class="notify-details">Caleb Flakelar commented on Admin<small class="text-muted">1 min ago</small></p>
				</a><!--/ dropdown-item-->
				<a href="javascript:void(0);" class="dropdown-item notify-item">
				 <div class="notify-icon bg-success"><i class="fa fa-user-plus"></i></div>
				 <p class="notify-details">Grace Flake followed you.<small class="text-muted">5 hours ago</small></p>
				</a><!--/ dropdown-item-->
				<a href="javascript:void(0);" class="dropdown-item notify-item">
				 <div class="notify-icon bg-success"><i class="fa fa-heart"></i></div>
				 <p class="notify-details">Carlos Crouch liked your photo.<small class="text-muted">3 days ago</small></p>
				</a><!--/ dropdown-item-->
				<a href="javascript:void(0);" class="dropdown-item notify-item">
				 <div class="notify-icon bg-success"><i class="fa fa-comment"></i></div>
				 <p class="notify-details">Caleb Flakelar commented on Admin<small class="text-muted">4 days ago</small></p>
				</a><!--/ dropdown-item-->
				<a href="javascript:void(0);" class="dropdown-item notify-item">
				 <div class="notify-icon bg-success"><i class="fa fa-user-plus"></i></div>
				 <p class="notify-details">Maureen Hilda followed you.<small class="text-muted">7 days ago</small></p>
				</a><!--/ dropdown-item-->
				<a href="javascript:void(0);" class="dropdown-item notify-item">
				 <div class="notify-icon bg-success"><i class="fa fa-heart"></i></div>
				 <p class="notify-details">Carlos Crouch liked your photo.<small class="text-muted">13 days ago</small></p>
				</a><!--/ dropdown-item-->
			   </div><!--/ .Slim-->
			   <div class="slimScrollBar" style="background: rgb(158, 165, 171) none repeat scroll 0% 0%; width: 8px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;"></div>
			   <div class="slimScrollRail" style="width: 8px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>
			  </div><!--/ .slimscroll-->
			 </div><!--/ .slimScrollDiv-->
			 <a href="photo_notifications.html" class="dropdown-item text-center notify-all">
			  View all <i class="fa fa-arrow-right"></i>
			 </a><!-- All-->
            </div><!--/ dropdown-menu-->
		   </li>

		   <li class="dropdown notification-list">
			<a class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
			 <i class="fa fa-envelope noti-icon"></i>
			 <span class="badge badge-success badge-pill noti-icon-badge">6</span>
			</a>
			<div class="dropdown-menu dropdown-menu-right dropdown-lg dropdown-new">
             <div class="dropdown-item noti-title">
			  <h6 class="m-0">
			   <span class="float-right">
			    <a href="" class="text-dark"><small>Clear All</small></a> 
			   </span>Chat
			  </h6>
			 </div>

			 <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 416.983px;">
			  <div class="slimscroll" style="max-height: 230px; overflow: hidden; width: auto; height: 416.983px;">
			   <div id="Slim2">
				<a href="javascript:void(0);" class="dropdown-item notify-item nav-user">
				 <div class="notify-icon"><img src="assets/img/users/1.jpg" class="img-responsive img-circle" alt=""> </div>
				 <p class="notify-details">Cristina Pride</p>
				 <p class="text-muted font-13 mb-0 user-msg">Hi, How are you? What about our next meeting</p>
				</a><!--/ dropdown-item-->
				<a href="javascript:void(0);" class="dropdown-item notify-item nav-user">
				 <div class="notify-icon"><img src="assets/img/users/2.jpg" class="img-responsive img-circle" alt=""> </div>
				 <p class="notify-details">Sam Garret</p>
				 <p class="text-muted font-13 mb-0 user-msg">Yeah everything is fine</p>
				</a><!--/ dropdown-item-->
				<a href="javascript:void(0);" class="dropdown-item notify-item nav-user">
				 <div class="notify-icon"><img src="assets/img/users/3.jpg" class="img-responsive img-circle" alt=""> </div>
				 <p class="notify-details">Karen Robinson</p>
				 <p class="text-muted font-13 mb-0 user-msg">Wow that's great</p>
				</a><!--/ dropdown-item-->
				<a href="javascript:void(0);" class="dropdown-item notify-item nav-user">
				 <div class="notify-icon"><img src="assets/img/users/4.jpg" class="img-responsive img-circle" alt=""> </div>
				 <p class="notify-details">Sherry Marshall</p>
				 <p class="text-muted font-13 mb-0 user-msg">Hi, How are you? What about our next meeting</p>
				</a><!--/ dropdown-item-->
				<a href="javascript:void(0);" class="dropdown-item notify-item nav-user">
				 <div class="notify-icon"><img src="assets/img/users/5.jpg" class="img-responsive img-circle" alt=""> </div>
				 <p class="notify-details">Shawn Millard</p>
				 <p class="text-muted font-13 mb-0 user-msg">Yeah everything is fine</p>
				</a><!--/ dropdown-item-->
			   </div><!--/ .Slim-->
			   <div class="slimScrollBar" style="background: rgb(158, 165, 171) none repeat scroll 0% 0%; width: 8px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;"></div>
			   <div class="slimScrollRail" style="width: 8px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>
			  </div><!--/ slimscroll-->
			 </div> <!--/ slimScrollDiv-->
			 <a href="photo_chat.html" class="dropdown-item text-center notify-all">
			  View all <i class="fa fa-arrow-right"></i>
			 </a>
            </div><!--/ dropdown-menu-->
		   </li>
		  
		 <li class="dropdown mega-avatar">
		  <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
		   <span class="avatar w-32">
<%
try {
Class.forName("com.mysql.jdbc.Driver").newInstance();
Connection CON1 = java.sql.DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/platzer", "root", "");
String sql1 = " SELECT * FROM usuario WHERE IDUSUARIO = '"+usuario.getAttribute("id_usuario")+"' ";
PreparedStatement PS1 = CON1.prepareStatement(sql1);
ResultSet RS1 = PS1.executeQuery();
while(RS1.next()) {
out.print("<img src="+RS1.getString("foto")+" class='img-resonsive img-circle' width='25' height='25' alt='...'></span>");         
 

%>


<!-- hidden-xs hides the username on small devices so only the image appears. -->
<span class="hidden-xs">

                       
<%                            
out.print(""+RS1.getString("nombre")+""+" "+""+RS1.getString("apaterno")+"");         
} //cierre while datos de arriba
CON1.close();
} catch (Exception e3) {
System.out.println("error de foto"+e3);
}
%>
                       
		   </span>
		  </a>
<div class="dropdown-menu w dropdown-menu-scale pull-right">
<a class="dropdown-item" href="#" onclick="DinamicoDiv('perfil');"><span>Perfil</span></a> 

<div class="dropdown-divider"></div>
<a class="dropdown-item" href="#" onclick="DinamicoDiv('ingresarvisita');"><span>Ingresar Visita</span></a> 
<a class="dropdown-item" href="#" onclick="DinamicoDiv('favoritos');"><span>Favoritos</span></a> 		   
<a class="dropdown-item" href="#" onclick="DinamicoDiv('lista');">Lista de Deseos</a> 
<div class="dropdown-divider"></div>

<a class="dropdown-item" href="index.jsp">Cerrar Sesión</a>

</div>
		 </li><!-- /navbar-item -->	
		 
		 </ul><!-- /.sign-in -->   
		</div><!-- /.nav-right -->
       </div><!-- /.container -->
      </nav><!-- /.navbar -->
     </header><!-- Page Header --> 
  
                </td>
            </tr>        
        </table>
        
        <table width="100%" border="1">
            
            <tr>
                <section class="nav-sec">
	  <div class="d-flex justify-content-between">
	   <div class="p-2 nav-icon-lg dark-black">
               <a class="nav-icon" href="#" onclick="DinamicoDiv('inicio'); mostrarComentarios(); peticionPublicacion();"><em class="fa fa-home"></em>
		<span>Inicio</span>
	   </a>
	   </div>
	   <div class="p-2 nav-icon-lg clean-black">
               <a class="nav-icon" href="#" onclick="DinamicoDiv('tours');mostrarTour();"><em class="fa fa-crosshairs"></em>
		<span>Tours</span>
	   </a>
	   </div>
	   <div class="p-2 nav-icon-lg dark-black">
	   <a class="nav-icon" href="#" onclick="DinamicoDiv('subidas');"><em class="fab fa-instagram"></em>
		<span>Subir</span>
	   </a>
	   </div>
	   <div class="p-2 nav-icon-lg clean-black">
               <a class="nav-icon" href="#" onclick="DinamicoDiv('boletin');mostrarBoletin();"><em class="fa fa-align-left"></em>
		<span>Boletín Semanal</span>
	   </a>
	   </div>
	   <div class="p-2 nav-icon-lg dark-black">
	   <a class="nav-icon" href="#" onclick="DinamicoDiv('perfil');peticionPublicacionPerfil();"><em class="fa fa-user"></em>
		<span>Perfil</span>
	   </a>
	   </div>
	  </div>
	</section>                               

        
        <table border="1" width="100%">
            <tr>
                <% // Espacio para Publicaciones  %>   
<td colspan="5"><div id="inicio" align="center">
        <div id="publicacionesFer">
        </div>   
</div>   
         <% // Espacio para Publicaciones  %>   <% // Espacio para Publicaciones  %>   <% // Espacio para Publicaciones  %>              
             <div id="tours" style="display: none">
                        
     <!-- ==============================================
	 Scripts
	 =============================================== -->
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/base.js"></script>
	<script src="assets/plugins/slimscroll/jquery.slimscroll.js"></script>
	<script>
	$('#Slim,#Slim2').slimScroll({
	        height:"auto",
			position: 'right',
			railVisible: true,
			alwaysVisible: true,
			size:"8px",
		});		
	</script>

                    </div>
                    <div id="subidas" style="display: none">
                        <!-- ==============================================
	 SUBIR
	 =============================================== --> 
	 <section class="upload">
	  <div class="container">
	  
	   <div class="row">
	    <div class="col-lg-12">  
		
	  <!-- <div class="box">
		  <form>
		   <textarea class="form-control no-border" rows="3" placeholder="Type something..."></textarea>
		  </form>
		  <div class="box-footer clearfix">
		   <button class="kafe-btn kafe-btn-mint-small pull-right btn-sm">Upload</button>
		   <ul class="nav nav-pills nav-sm">
			<li class="nav-item"><a class="nav-link" href=""><i class="fa fa-camera text-muted"></i></a></li>
			<li class="nav-item"><a class="nav-link" href=""><i class="fa fa-video text-muted"></i></a></li>
		   </ul>
		  </div>
		 </div>	-->
          
          
          
            <div id="lugar"  class="row">
                        <br/>
                        <center>
                        <table>                            
                            <tr>                                
                                <td width="50%">                                    
                                    <aside>
                                        <input type="button" value="Registra un Lugar" class="btn btn-theme btn-block" onclick="mostrarRegLugar();">
                                        <br><br>
                                        <input type="button" value="Registrar Visita" class="btn btn-theme btn-block" onclick="comboLugar();">
                                       
                                    </aside>  
                                    <div id="loslugares">                                                                                
                                    </div>
                                </td>                                                                                                                                                                                                                                                                                   
                            </tr>                            
                        </table>
                            </center>
                    </div>
		 
		</div>
	   </div>
	  
	   <div class="row one-row">
	
	   </div>
	   
	   <div class="row">

        <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
      
        </div>

        <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
      
        </div>

        <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
      
        </div>
		
       </div><!--/ row-->	   
	   
	   <div class="row">

        <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
     
        </div>

        <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
         
        </div>

        <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
       
        </div>
		
       </div><!--/ row-->	
	   
	   <div class="row">

        <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
     
        </div>

        <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
       
        </div>

        <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
        
        </div>
		
       </div><!--/ row-->	
	  
	   
	  </div><!--/ container -->
	 </section><!--/ newsfeed -->
     <!-- ==============================================
	 Scripts
	 =============================================== -->
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/base.js"></script>
	<script src="assets/plugins/slimscroll/jquery.slimscroll.js"></script>
	<script>
	$('#Slim,#Slim2').slimScroll({
	        height:"auto",
			position: 'right',
			railVisible: true,
			alwaysVisible: true,
			size:"8px",
		});		
	</script>


                    </div>
                    <div id="boletin" style="display: none" align="center" class="col-xs-12">
                        
                 
     <!-- ==============================================
	 Scripts
	 =============================================== -->
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/base.js"></script>
	<script src="assets/plugins/slimscroll/jquery.slimscroll.js"></script>
	<script>
	$('#Slim,#Slim2').slimScroll({
	        height:"auto",
			position: 'right',
			railVisible: true,
			alwaysVisible: true,
			size:"8px",
		});		
	</script>

	

                    </div>
                    <div id="perfil" style="display: none"><br/>
                        <!-- ==============================================
	 PERFIL
	 =============================================== --> 
	 <section class="profile-two">
	  <div class="container-fluid">
	   <div class="row">

		<div class="col-lg-3">
         <aside id="leftsidebar" class="sidebar">		  
		  <ul class="list">
           <li>
			<div class="user-info">
			 <div class="image">
		      <a href="#">
                            <%
           
                try {
Class.forName("com.mysql.jdbc.Driver").newInstance();
Connection CON2 = java.sql.DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/platzer", "root", "");
String sql2 = " SELECT * FROM usuario WHERE IDUSUARIO = '"+usuario.getAttribute("id_usuario")+"' ";
PreparedStatement PS2 = CON2.prepareStatement(sql2);
ResultSet RS2 = PS2.executeQuery();
while(RS2.next()) {
out.print("<img src="+RS2.getString("foto")+" class='img-responsive img-circle' alt='User'>");
 
%>

                           <span class="online-status online"></span>
			  </a>
			 </div>
		     <div class="detail">
<h4>
<% 
    out.print(""+RS2.getString("nombre")+"");
}//while de nombre 
CON2.close();
} catch (Exception e3) {
System.out.println("error de foto"+e3);
}

%>

</h4>
<br>
<form enctype='multipart/form-data' id='formuFoto'>
    <input class=input type="file"  placeholder="Imagen" name="imagenPerfil" id="imagenPerfil">
    <input class=btn__submit type="button" value="Cambiar foto" onclick="peticionCambiarFoto();">
</form>


<br>                     
</div>
<div class="row">
<div class="col-12">
</div>                                
</div>
</div>
           </li>
           <li>
                             
           </li>
           <li>
<input type="button" value="Editar mis Datos" class="btn btn-theme btn-block" onclick="DinamicoDiv('editardatos')">
<input type="button" value="Dar de baja mi cuenta" class="btn btn-theme btn-block" onclick="DinamicoDiv('dardebaja')">
<input type="button" value="Viajes favoritos" class="btn btn-theme btn-block" onclick="DinamicoDiv('viajesfavoritos'); peticionverviajesfavoritos();">
<input type="button" value="Proximos viajes" class="btn btn-theme btn-block" onclick="DinamicoDiv('proximosviajes')">
<input type="button" value="Cerrar sesión" class="btn btn-theme btn-block" onclick="peticioncierresesion();">

           </li>
          </ul>
         </aside>				
		</div><!--/ col-lg-3-->
		
<div class="col-lg-6" id="fotoPerfil" style="background: #fff;">
<div class="row">

    <%//Aqui van las imagenes de publicacion>%>
  
 <%    
    try {

Class.forName("com.mysql.jdbc.Driver").newInstance();
Connection CON3 = java.sql.DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/platzer", "root", "");
String sql3 = "select lu.nombre,vi.imagen,vi.fecha,vi.costo,vi.DESCRIPCION,vi.CALIFICACION,pu.IDPUBLICACION\n" +
"from visita vi\n" +
"inner join publicacion pu\n" +
"on vi.IDVISITA=pu.IDVISITA\n" +
"inner join lugar lu\n" +
"on lu.IDLUGAR=vi.IDLUGAR\n" +
"where vi.IDUSUARIO="+usuario.getAttribute("id_usuario")+"";
PreparedStatement PS3 = CON3.prepareStatement(sql3);
ResultSet RS3 = PS3.executeQuery();
while(RS3.next()) {
out.print("<div class=col-lg-6>");   
out.print("<a href=#myModal data-toggle=modal>");         
out.print("<div class='explorebox' ");         
out.print("style='background: linear-gradient( rgba(34,34,34,0.2), rgba(34,34,34,0.2)), url("+RS3.getString("imagen")+") no-repeat;");
out.print("background-size: cover;"); 
out.print("background-position: center center;"); 
out.print("-webkit-background-size: cover;"); 
out.print("-moz-background-size: cover;"); 
out.print("-o-background-size: cover;'>");  
out.print("<div class=explore-top>");   
out.print("<div class=explore-like><i class=fa fa-heart></i> <span>1499</span></div>");         
out.print("<div class=explore-circle pull-right><i class=far fa-bookmark></i></div>");         
out.print("</div>");
out.print("</div>"); 
out.print("</a>"); 
out.print("</div>"); 
out.print(""); 
out.print("");


} 
CON3.close();

} catch (Exception e3) {
System.out.println("error de editar datos consulta"+e3);
} 
%>
    
    
    <%//Aqui van las imagenes de publicacion>%> 
</div><!--/ row -->
</div>
<div class="col-lg-3">

<div class="suggestion-box full-width">

</div>	
</div>

</div><!--/ row-->	
</div><!--/ container -->
</section><!--/ profile -->
  
	 <!-- ==============================================
	 Modal Section
	 =============================================== -->
    
	   <div class="row" id="perfilPublicaciones">
	 
     
      </div><!--/ modal-dialog -->
  		 
	   
     <!-- ==============================================
	 Scripts
	 =============================================== -->
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/base.js"></script>
	<script src="assets/plugins/slimscroll/jquery.slimscroll.js"></script>
	<script>
	$('#Slim,#Slim2').slimScroll({
	        height:"auto",
			position: 'right',
			railVisible: true,
			alwaysVisible: true,
			size:"8px",
		});		
	</script>
                    </div>                    
                    
                  
                        

<div id="editardatos" style="display: none">
<br><br>
<form  action="http://localhost:8080/Platzer/fer/../servEditarDatos" method="post">
<% 

try {

Class.forName("com.mysql.jdbc.Driver").newInstance();
Connection CON3 = java.sql.DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/platzer", "root", "");
String sql3 = " SELECT * FROM usuario WHERE IDUSUARIO = '"+usuario.getAttribute("id_usuario")+"' ";
PreparedStatement PS3 = CON3.prepareStatement(sql3);
ResultSet RS3 = PS3.executeQuery();
while(RS3.next()) {
out.print(" <div class='form-group'>");   
out.print("  <input id='emailEditar' value='"+RS3.getString("nombre")+"' type='text' class='form-control' placeholder='Nombre'  disabled='disabled' onkeypress=\"return soloLetras(event);\">");         
out.print(" </div>");         
out.print(" <div class='form-group'>");
out.print("   <input type='text' value='"+RS3.getString("apaterno")+"' class='form-control' id='usuarioEditar'  placeholder='Paterno'  disabled='disabled' onkeypress=\"return soloLetras(event);\"> "); 
out.print(" </div>"); 
out.print(" <div class='form-group'>"); 
out.print(" <input type='password' value='"+RS3.getString("contrasena")+"' class='form-control' id='passwordEditar'   placeholder='Contraseña' disabled='disabled'> "); 
out.print(" </div>");      } 
CON3.close();

} catch (Exception e3) {
System.out.println("error de editar datos consulta"+e3);
} 
%>
<br>

</form>  

<input type="button" id="botonEdi" value="Editar mis datos" class="btn btn-theme btn-block" onclick="habilitarCajas();">
<input type="button" value="Volver a mi Perfil" class="btn btn-theme btn-block" onclick="DinamicoDiv('perfil')">

</div>

<div id="proximosviajes" style="display: none">
    
   <table border="3" width="100%">
       <br><br>
        <tr>
        <td width="20%">
            <input class="btn btn-default btn-theme"  type="submit" id="s1" name="enviar" value="CONSULTAR" onclick="peticionControladorInicioViajes();"><br><br>
            <input class="btn btn-default btn-theme"  type="submit" id="s2" name="enviar" value="REGISTRAR" onclick="peticionControladorRegistrarViaje();"><br><br>
       
        </td>
        <td>
            <div id="consultaViajes">
   
            </div>
            <div id="registrarViajes">
              
            </div>
       
        </td>
        </tr>
            </table>
    <br><br><br>
<input type="button" value="Volver a mi Perfil" class="btn btn-theme btn-block" onclick="DinamicoDiv('perfil')">

</div>







<div id="viajesfavoritos" style="display: none">
    <div id="todosviajesFavoritos">
        
    </div>
    <br><br><br>
<input type="button" value="Volver a mi Perfil" class="btn btn-theme btn-block" onclick="DinamicoDiv('perfil');peticionPublicacionPerfil();">

</div>





<div id="dardebaja" style="display: none">
<br><br><br><br><br>

<center>
¿Estas seguro de dar de baja tu cuenta?, si 
es asi haz click en el boton confirmar, si en algun momento,
deseas volver a utilizar tu cuenta, solo debes de inicar sesión de 
manera normal, para que se vuelva a activar tu cuenta.
</center> 
<br><br>
<input type="button" id="botonEdi" value="Confirmar" class="btn btn-theme btn-block" onclick="peticionControladorCuentaBaja();" >

</div>
                          
                    <div id="ingresarvisita" style="display: none">
                        Aqui va formulario para ingresar una visita
                    </div>
                    
                    <div id="buscar" style="display: none">
                      
                    </div>
                    
                    <div id="favoritos" style="display: none">
                        Aqui van los FavsS
                    </div>
                    
                    <div id="lista" style="display: none">
                        
                        <div class="col-sm-4">
                            <br/>
                            <input type="button" value="Agregara a mi Lista" class="btn btn-theme btn-block" onclick="mostrarRegLista();">                                        
                            <input type="button" value="Ver mi Lista" class="btn btn-theme btn-block" onclick="consultarLista();"> 
                        </div>
                        
                        <div id="gestionarLista" class="col-sm-8">
                            <div id="RegLista"></div>
                        </div>                                                                                                                      
                    </div>
                    
                    <div id="cotizar" style="display: none;margin:auto;" class="col-xs-12 col-lg-12 col-md-12 newsfeed">
                                <!--<div class="col-md-4"></div>-->
                                <div class="col-md-4">
                                
                                    <h2>Cotizaciones</h2>
                                    <h4>Completa el siguiente formulario:</h4><br/>
                                    <img src="assets/img/tours/cotizar.png" class="img-resonsive img-responsive" width="300" height="300">
                                    </div>
                                <div class="col-md-4">
                                    Nombre: <input type="text" class="form-control" id="nombreCotizar" readonly="readonly" value="<% out.print(session.getAttribute("nombre")); %>"><br/>
                                    Apellido: <input type="text" class="form-control" id="apellidoCotizar" readonly="readonly" value="<% out.print(session.getAttribute("apellido")); %>"><br/>
                                    Correo: <input type="text" class="form-control" id="correoCotizar" readonly="readonly" value="<% out.print(session.getAttribute("correo")); %>"><br/>
                                    Destino: <input type="text" class="form-control" id="destinoCotizar" placeholder="ej. Paris, Cancún"><br/>
                                    Fecha: <input type="date" id="fechaCotizar" class="form-control"><br/>
                                    Cantidad de Personas:<input type="number" max="20" min="1" value="1" class="form-control" id="cantidadCotizar"><br/>
                                </div>
                                <div class="col-md-4">
                                    
                                    Empresa:
                                    <SELECT NAME="aero" id="correoEmpresa0" CLASS="parameter_Select form-control" onchange="consultarCorreoE1();">
                                        <option>-Selecciona Empresa-</option>
                                                            <%
                                try {
                                    Class.forName("com.mysql.jdbc.Driver").newInstance();
                                    Connection CON = java.sql.DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/PLATZER", "root", "");

                                    String qrCate = "SELECT NOMBRE FROM EMPRESA;";
                                    PreparedStatement PS = CON.prepareStatement(qrCate);
                                    ResultSet RS = PS.executeQuery();

                                    if (!RS.next()) {
                                        out.println("<OPTION VALUE=\"0\" SELECTED>No hay datos</OPTION>");
                                    } else {
                                        do {
                                            out.println("<OPTION VALUE=\"" + RS.getString(1) + "\">" + RS.getString(1) + "</OPTION>");
                                        } while (RS.next());
                                    }
                                    System.out.println("Conexion realizada");
                                } catch (Exception e3) {
                                    out.println("<OPTION VALUE=\"0\" SELECTED>No hay datos</OPTION>");
                                    System.out.println("No hizo conexion");
                                    e3.printStackTrace(System.err);
                                }
                            %>
                            </SELECT><br/><br/>
                                    <div id="correoEmpresa1">
                                        
                                    </div>
                            Comentario: <textarea id="comentarioCotizar" rows="8" class="form-control" placeholder="¿Tienes algo que especificar? Escribelo aquí..."></textarea><br/><br/>
                            <center>
                                <input type="button" value="Cotizar" class="btn btn-theme btn-default" onclick="enviarCotizacion();">
                                <div id="enviarCotizacion"></div>
                            </center>
                                

                                </div>
                        </div>
                         
                         <div id="compraTour" style="display: none" class="col-xs-12">                             
                                 <br/>
                                 <div class="col-md-2">
                                     <h5><strong>Selecciona forma de pago:</strong></h5><br/><br/>                                                                
                                <button onclick="mostrarFormaPago('tarjeta');" class="btn btn-default btn-theme"><span>Tarjeta</span></button><br/><br/>
                                <button onclick="mostrarFormaPago('oxxo');" class="btn btn-default btn-theme"><span>Pago en OxxO</span></button><br/><br/>
                                <button onclick="DinamicoDiv('tours');" class="btn btn-default btn-theme"><span>Cancelar</span></button><br/>
                                <input type="hidden" id="idusuarioCT" class="input-field" value="<% out.print(usuario.getAttribute("idusuario")); %>">
                                <!--<input type="text" maxlength="3" #0fc19e onkeypress="return soloNumeros(event)" class="input-sm" placeholder="Código de Seguridad"><br/><br/>
                                <input type="date" class="date"> -->
                                 </div>                         
                         <div id="gestionarCT" class="col-md-8">
                                     
                                 </div>
                         </div>
                </td>
            </tr>
            
            <tr>
                <td>
                    <!-- ==============================================
     PIE DE PAGINA
     =============================================== -->
     <footer id="colophon" class="site-footer">
      <div id="footer-menu">
	   <ul>
	    <li><a href="#">About</a></li>
		<li><a href="#">How it works</a></li>
		<li><a href="#">Contact</a>  </li>
	   </ul>
	   <div class="footer-social-icons">
        <div class="social-icons">
		 <a href="#"><i class="fab fa-instagram"></i></a>
		 <a href="#"><i class="fab fa-facebook"></i></a>
		 <a href="#"><i class="fab fa-twitter"></i></a>
		</div>  
       </div>
	   <p class="footer-copyright">© 2018 PLatzer Inc.</p>
	  </div>
     </footer>
                </td>
            </tr>
        </table>
                    <body onload="peticionPublicacion();">
    </body>        
</html>
